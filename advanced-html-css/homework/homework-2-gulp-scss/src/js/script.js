const servicesList = document.querySelectorAll(".hamburger-menu-start");
const wrapperHamburgerMenu = document.querySelector(".wrapper-hamburger-menu");
const navHeader = document.querySelector(".nav-header");

function showImgBurgerMenu() {
    servicesList.forEach(element => {
        element.classList.toggle("hamburger-menu-click");
    });
}

function showBurgerMenu() {
    navHeader.classList.toggle("nav-header-click");
}

wrapperHamburgerMenu.addEventListener("click", function (event) {
    showImgBurgerMenu();
    showBurgerMenu();
})

