const gulp = require('gulp');
const autoprefixer = require('gulp-autoprefixer'); // Префикс CSS с помощью Autoprefixer
const concat = require('gulp-concat'); // Объединяет файлы
const uglify = require('gulp-uglify');  // Minify JavaScript
const htmlmin = require('gulp-htmlmin');  // gulp плагин для минимизации HTML.
const sass = require('gulp-sass')(require('sass')); // Плагин Sass для Gulp.
const imagemin = require('gulp-imagemin');  // Сократите изображения PNG, JPEG, GIF и SVG с помощью imagemin
const cssmin = require('gulp-cssmin'); // Minify Css
const rename = require('gulp-rename');
const del = require('delete');
//////////////
const watch = require('gulp-watch'); //расширение возможностей watch
const connect = require('gulp-connect'); //livereload

/////////// no
const jsMinify = require('gulp-js-minify');  // Использовала др. плагин
const gulpClean = require('gulp-clean');   // Использовала др. плагин
const cleanCss = require('gulp-clean-css');
const browserSync = require('browser-sync').create();
const scss = require('gulp-scss');  // Плагин Scss для Gulp.

// +
function buildCSS() {
    return gulp.src('src/scss/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(concat('style.css'))

        .pipe(autoprefixer({
            cascade: false
        }))
        .pipe(cssmin())
        .pipe(rename({ suffix: '.min' }))
        .pipe(gulp.dest('dist'))
}

// +
function buildHTML() {
    return gulp.src('*.html')
        .pipe(htmlmin({ collapseWhitespace: true }))
        .pipe(gulp.dest('dist'))
}

// +
function buildJS() {
    return gulp.src('src/js/*.js')
        .pipe(uglify())
        .pipe(rename({ suffix: '.min' }))
        .pipe(gulp.dest('dist'))
}

// +
function buildImages() {
    return gulp.src('src/img/*')
        .pipe(imagemin())
        .pipe(gulp.dest('dist/img'))
}
////////////////
function buildFonts() {
    return gulp.src('src/fonts/*')
        .pipe(gulp.dest('dist/fonts'))
}

// + ?????????
function clean() {
    return del('dist/**', { force: true });
}

function build() {
    return gulp.series([
        clean,
        gulp.parallel([
            buildImages,
            buildCSS,
            buildHTML,
            buildJS,
            buildFonts
        ])
    ])
}

// function start() {
//     gulp.watch('src/**/*', build())
// }


//////////////////////////////////
function browserSyncDev() {
    browserSync.init({
        server: {
            baseDir: "dist/"
        },
        port: 8080,
        notify: false
    });
    gulp.watch('src/scss/*.scss', buildCSS).on('change', browserSync.reload);
    gulp.watch('*.html', buildHTML).on('change', browserSync.reload);
    gulp.watch('src/js/*.js', buildJS).on('change', browserSync.reload);
    gulp.watch('src/img/*', buildImages).on('change', browserSync.reload);
    gulp.watch('src/fonts/*', buildFonts).on('change', browserSync.reload);
}

function dev() {
    return gulp.series([
        clean,
        buildImages,
        buildCSS,
        buildHTML,
        buildJS,
        buildFonts,
        browserSyncDev
    ])
}

////////////////////////////////////
exports.buildHTML = buildHTML;
exports.buildCSS = buildCSS;
exports.buildJS = buildJS;
exports.buildFonts = buildFonts;
exports.buildImages = buildImages;
exports.clean = clean;
exports.dev = dev();
exports.default = build();






/////////////////////////////////////

// var gulp = require('gulp');
// var less = require('gulp-less');
// var babel = require('gulp-babel');
// var concat = require('gulp-concat');
// var uglify = require('gulp-uglify');
// var rename = require('gulp-rename');
// var cleanCSS = require('gulp-clean-css');
// var del = require('del');

// var paths = {
//     styles: {
//         src: 'src/styles/**/*.less',
//         dest: 'assets/styles/'
//     },
//     scripts: {
//         src: 'src/scripts/**/*.js',
//         dest: 'assets/scripts/'
//     }
// };

// /* Not all tasks need to use streams, a gulpfile is just another node program
//  * and you can use all packages available on npm, but it must return either a
//  * Promise, a Stream or take a callback and call it
//  */
// function clean() {
//     // You can use multiple globbing patterns as you would with `gulp.src`,
//     // for example if you are using del 2.0 or above, return its promise
//     return del(['assets']);
// }

// /*
//  * Define our tasks using plain functions
//  */
// function styles() {
//     return gulp.src(paths.styles.src)
//         .pipe(less())
//         .pipe(cleanCSS())
//         // pass in options to the stream
//         .pipe(rename({
//             basename: 'main',
//             suffix: '.min'
//         }))
//         .pipe(gulp.dest(paths.styles.dest));
// }

// function scripts() {
//     return gulp.src(paths.scripts.src, { sourcemaps: true })
//         .pipe(babel())
//         .pipe(uglify())
//         .pipe(concat('main.min.js'))
//         .pipe(gulp.dest(paths.scripts.dest));
// }

// function watch() {
//     gulp.watch(paths.scripts.src, scripts);
//     gulp.watch(paths.styles.src, styles);
// }

// /*
//  * Specify if tasks run in series or parallel using `gulp.series` and `gulp.parallel`
//  */
// var build = gulp.series(clean, gulp.parallel(styles, scripts));

// /*
//  * You can use CommonJS `exports` module notation to declare tasks
//  */
// exports.clean = clean;
// exports.styles = styles;
// exports.scripts = scripts;
// exports.watch = watch;
// exports.build = build;
// /*
//  * Define default task that can be called by just running `gulp` from cli
//  */
// exports.default = build;
// Use latest JavaScript version in your gulpfile
// Most new versions of node support most features that Babel provides, except the import /export syntax. When only that syntax is desired, rename to gulpfile.esm.js, install the [esm][esm-module] module, and skip the Babel portion below.

// Node already supports a lot of ES2015 + features, but to avoid compatibility problems we suggest to install Babel and rename your gulpfile.js to gulpfile.babel.js.

// npm install--save - dev @babel/register @babel/core @babel/preset-env
// Then create a.babelrc file with the preset configuration.

// {
//     "presets": ["@babel/preset-env"]
// }
// And here's the same sample from above written in ES2015+.

// import gulp from 'gulp';
// import less from 'gulp-less';
// import babel from 'gulp-babel';
// import concat from 'gulp-concat';
// import uglify from 'gulp-uglify';
// import rename from 'gulp-rename';
// import cleanCSS from 'gulp-clean-css';
// import del from 'del';

// const paths = {
//     styles: {
//         src: 'src/styles/**/*.less',
//         dest: 'assets/styles/'
//     },
//     scripts: {
//         src: 'src/scripts/**/*.js',
//         dest: 'assets/scripts/'
//     }
// };

// /*
//  * For small tasks you can export arrow functions
//  */
// export const clean = () => del(['assets']);

// /*
//  * You can also declare named functions and export them as tasks
//  */
// export function styles() {
//     return gulp.src(paths.styles.src)
//         .pipe(less())
//         .pipe(cleanCSS())
//         // pass in options to the stream
//         .pipe(rename({
//             basename: 'main',
//             suffix: '.min'
//         }))
//         .pipe(gulp.dest(paths.styles.dest));
// }

// export function scripts() {
//     return gulp.src(paths.scripts.src, { sourcemaps: true })
//         .pipe(babel())
//         .pipe(uglify())
//         .pipe(concat('main.min.js'))
//         .pipe(gulp.dest(paths.scripts.dest));
// }

// /*
//  * You could even use `export as` to rename exported tasks
//  */
// function watchFiles() {
//     gulp.watch(paths.scripts.src, scripts);
//     gulp.watch(paths.styles.src, styles);
// }
// export { watchFiles as watch };

// const build = gulp.series(clean, gulp.parallel(styles, scripts));
// /*
//  * Export a default task
//  */
// export default build;
// Incremental Builds
// You can filter out unchanged files between runs of a task using the gulp.src function's since option and gulp.lastRun:

// const paths = {
//     ...
//     images: {
//         src: 'src/images/**/*.{jpg,jpeg,png}',
//         dest: 'build/img/'
//   }
// }

// function images() {
//     return gulp.src(paths.images.src, { since: gulp.lastRun(images) })
//         .pipe(imagemin({ optimizationLevel: 5 }))
//         .pipe(gulp.dest(paths.images.dest));
// }

// function watch() {
//     gulp.watch(paths.images.src, images);
// }