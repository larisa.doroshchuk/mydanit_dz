import Product from '../product/Product';
import ModalOk from '../modal_ok/ModalOk';
import './List.css';
import PropTypes from 'prop-types'
import React from 'react';
import { useSelector } from 'react-redux';
//+

const List = (props) => {
    const { handleClick, handleClickFavorite, handleClickClose, handleClickOk, handleClickProduct } = props;
    let productList = useSelector((state) => state.productList);


    return (

        <>
            <section className="list-container">

                {productList.map((item) => (

                    <React.Fragment key={item.article}>

                        <div className="list-item">
                            <Product
                                name={item.name}
                                price={item.price}
                                color={item.color}
                                article={item.article}
                                url={item.url}
                                handleClick={handleClick}
                                handleClickClose={handleClickClose}
                                handleClickOk={handleClickOk}
                                handleClickFavorite={handleClickFavorite}
                                handleClickProduct={handleClickProduct}

                            />


                        </div>
                    </React.Fragment>

                ))}


                <ModalOk
                    onClick={handleClickClose}
                />

            </section>

        </>
    )



}


List.propTypes = {
    productList: PropTypes.array,
    handleClick: PropTypes.func,
    handleClickClose: PropTypes.func,
    handleClickOk: PropTypes.func,
    handleClickFavorite: PropTypes.func,
    handleClickProduct: PropTypes.func,
}

export default List
