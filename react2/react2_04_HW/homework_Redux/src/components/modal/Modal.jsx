import React, { } from 'react'
import './Modal.css'
import PropTypes from 'prop-types'
import { useSelector } from 'react-redux';
//+

const Modal = (props) => {

    const modalContent = useSelector((state) => state.modalContent);
    const modalActive = useSelector((state) => state.modalActive);

    const headerText = "Are you sure you want to remove this item from your cart?"
    const { onClick, handleClickOk } = props;
    return (
        <>
            <div onClick={onClick} className={modalActive ? "modal-container activ-modal-container" : "modal-container"}>
                <div className="modal-wrapper" onClick={e => e.stopPropagation()}>
                    <header className="header-modal">
                        <h3 className="modal-title">{headerText}</h3>
                        <button onClick={onClick} className="btn-close btn-close-white btn-visually" aria-label="Close" >x</button>
                    </header>

                    <div className="modal-body">
                        <div className="modal-text">{modalContent.name}</div>

                        <div className="button-container">
                            <button id={modalContent.article} className="button-ok button-modal" onClick={handleClickOk} data-article={modalContent.article}>Ok</button>
                            <button className="button-cancel button-modal" onClick={onClick}>Cancel</button>
                        </div>
                    </div>

                </div>
            </div>
        </>
    )

}


Modal.propTypes = {
    onClick: PropTypes.func,
    modalActive: PropTypes.any,
    handleClickOk: PropTypes.func,
}


export default Modal

