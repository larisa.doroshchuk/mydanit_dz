
import React, { } from 'react'
import PropTypes from 'prop-types'
import './Button.css'
// +


const Button = (props) => {
    const text = "Add to cart";
    const style = { backgroundColor: "grey" }
    const { onClick, article } = props;


    return (
        <>
            <button id={article} onClick={onClick} style={style} className="button-someone">{text}</button>
        </>
    )

}


Button.propTypes = {
    backgroundColor: PropTypes.string,
    text: PropTypes.string,
    onClick: PropTypes.func,
    article: PropTypes.number
}

Button.defaultProps = {
    text: ""
}

export default Button
