import React, { } from 'react'
import List from '../list/List'
import { useSelector } from 'react-redux';

const Home = (props) => {

  // products: productList, modalActive,  favoritesList,
  const { handleClick, handleClickClose, handleClickOk, handleClickFavorite, cartList, handleClickProduct } = props;
  // let modalActive = useSelector((state) => state.modalActive);


  return (
    <>
      <List
        handleClick={handleClick}
        handleClickClose={handleClickClose}
        handleClickOk={handleClickOk}
        handleClickFavorite={handleClickFavorite}
        handleClickProduct={handleClickProduct}

      />
    </>
  )


}

export default Home


