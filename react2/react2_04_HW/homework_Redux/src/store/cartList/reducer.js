const reducer = (state = [], action) => {
    switch (action.type) {
        case 'ADD_CARTLIST': {
            const item = state.filter((cart) => cart.id === action.payload.cart);
            const newItem = item.length === 0 ? { "id": +action.payload.cart, "quantity": 0 } : item[0];
            newItem.quantity++;
            const newState = [...state.filter((cart) => cart.id !== action.payload.cart), newItem];
            localStorage.setItem('cartList', JSON.stringify(newState));
            return newState;
        }
        case 'REMOVE_CARTLIST': {
            const cartListNew = state.filter((elem) => elem.id !== action.payload.id)
            localStorage.setItem('cartList', JSON.stringify(cartListNew));
            return cartListNew;
        }
        case 'INIT_CARTLIST': {
            return action.payload.cart ? [...action.payload.cart] : [];
        }
        default: {
            return state
        }
    }
}

export default reducer

