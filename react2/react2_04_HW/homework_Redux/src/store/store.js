import { legacy_createStore as createStore, combineReducers, compose, applyMiddleware } from 'redux';
import productsReducer from './products/reducer';
import favoritesReducer from './favorites/reducer';
import cartListReducer from './cartList/reducer';
import modalActiveReducer from './modalActive/reducer';
import modalContentReducer from './modalContent/reducer';
import thunk from 'redux-thunk';


const initialState = {
    productList: [],
    favoritesList: [],
    cartList: [],
    modalActive: false,
    modalContent: {
        name: '',
        article: 0
    }
}

const reducer = combineReducers({
    productList: productsReducer,
    favoritesList: favoritesReducer,
    cartList: cartListReducer,
    modalActive: modalActiveReducer,
    modalContent: modalContentReducer
})

const devTools = window.__REDUX_DEVTOOLS_EXTENSION__ ? window.__REDUX_DEVTOOLS_EXTENSION__() : (f => f)

const store = createStore(
    reducer,
    initialState,
    compose(applyMiddleware(thunk), devTools),
)


export default store
