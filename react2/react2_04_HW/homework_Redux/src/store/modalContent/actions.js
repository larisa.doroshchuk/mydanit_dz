export const switchContentModal = content => {
    return {
        type: 'SWITCH_CONTENT_MODAL',
        payload: { content }
    }
}
