import React, { Component } from 'react'
import Product from '../product/Product';
import './List.css'
import PropTypes from 'prop-types'


class List extends Component {

    render() {
        const { productList, favoritesList, handleClick, modalActive, handleClickClose, handleClickOk, handleClickFavorite } = this.props

        return (
            
            <>
                <section className="list-container">

                {productList.map((item)=> (
                    <React.Fragment key={item.article}>
                        <div className="list-item">
                            <Product 
                                name={item.name}
                                price={item.price}
                                color={item.color}
                                article={item.article}
                                url={item.url}
                                handleClick={handleClick}
                                modalActive={modalActive} 
                                handleClickClose={handleClickClose}
                                handleClickOk={handleClickOk}
                                handleClickFavorite={handleClickFavorite}
                                favoritesList={favoritesList}
                                
                            />                            
                        </div>
                    </React.Fragment>

                ))}

                </section>
              
            </>
        )
    }
}


List.propTypes = {
    productList: PropTypes.array,
    favoritesList: PropTypes.object,
    handleClick: PropTypes.func,
    modalActive: PropTypes.any,
    handleClickClose: PropTypes.func,
    handleClickOk: PropTypes.func,
    handleClickFavorite: PropTypes.func,
}

export default List