import React, { Component } from 'react'
import Button from '../button/Button'
import Modal from '../modal/Modal'
import PropTypes from 'prop-types'
import './Product.css'

class Product extends Component {
    
    render() {
        const { name, price, color, article, url, handleClick, modalActive, handleClickClose, handleClickOk, handleClickFavorite, favoritesList } = this.props

        return (
            <>
                <section className="product-container">
                    <div className="product-favorites">
                        <p className="product-id">ID - {article}</p>
                        <a href="#" className="select-favorites" onClick={handleClickFavorite}>
                            <span id={"favorite-" + article} className={typeof favoritesList["favorite-" + article] !== "undefined" && favoritesList["favorite-" + article] === true ?  "favorites-icon favorites-icon89" : "favorites-icon favorites-icon88" }></span>
                        </a>
                    </div>
                    <div className="foto-product">
                        <img className="img-product" src={url} alt={name} />
                    </div>
                    <h3 className="product-title">{name}</h3>
                    <p className="product-color">Color: {color}</p>
                    <p className="product-price">$ {price}</p>
                    <div className="product-button">
                        <Button 
                            backgroundColor="green"
                            text="Add to cart"
                            onClick={handleClick} 
                            article={article}
                        />
                        <Modal 
                            modalActive={modalActive} 
                            headerText="Add this item to cart?" 
                            text={name} 
                            onClick={handleClickClose}
                            article={article}
                            handleClickOk={handleClickOk}
                        />
                    </div>      

                </section>
           </>
        )
    }
}

Product.propTypes = {
    name: PropTypes.string,
    price: PropTypes.number,
    color: PropTypes.string,
    article: PropTypes.number,
    url: PropTypes.string,
    handleClick: PropTypes.func,
    modalActive: PropTypes.any,
    handleClickClose: PropTypes.func,
    handleClickOk: PropTypes.func,
    handleClickFavorite: PropTypes.func,
    favoritesList: PropTypes.object,

}

export default Product