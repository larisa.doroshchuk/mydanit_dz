import {Component} from 'react';
import Button from './components/button/Button';
import Modal from './components/modal/Modal';


const content = [
  {
    id: 'modalID1',
    titleModal: 'Do you want to delete this file?',
    contentModal: "Once you delete this file, won't be possible to undo this action. Are you sure you want to delete it?"
  },
  {
    id: 'modalID2',
    titleModal: "Do you want to add this file?",
    contentModal: "Is this fight right for you? Confirm that you want to debug this file."
  }
]

class App extends Component {

  constructor(props) {
    super(props)

    this.state = {
      modalActive: false,
      modalContent: content[0]
    }
  }
   
  
  handleClick = (event) => {
    this.setState({ modalActive: true, modalContent:content.find(item => item.id === event.target.dataset.modalId) }) ;
  }

  handleClickClose = (event) => {
    this.setState({ modalActive: false });
  }


  render() {

    return (
      <>
        <div className="container-button">
          <Button onClick={this.handleClick} 
                  colorButton="#db6666"
                  titleButton="Open first modal"
                  modalId="modalID1"
          />
          <Button onClick={this.handleClick} 
                  colorButton="#5dbb5d"
                  titleButton="Open second modal"
                  modalId="modalID2"
          />
        </div>

        <Modal
          modalActive={this.state.modalActive}
          headerText={this.state.modalContent.titleModal} 
          text={this.state.modalContent.contentModal}
          onClick={this.handleClickClose}
        />
      </>
    )
  }
}

export default App;
