import { Component} from 'react'
import './Button.scss'

class Button extends Component {


    render() {
        const { onClick, colorButton, titleButton, modalId} = this.props
        const style={backgroundColor: colorButton}
        return (
            <>
            <div className="button-container">
                <button className="btn-all" data-modal-id={modalId} onClick={onClick} style={style}>{titleButton}</button>
            </div>

            </>
        );
    }

}

export default Button;