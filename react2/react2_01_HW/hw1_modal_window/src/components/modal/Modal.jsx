import React, { Component } from 'react'
import './Modal.scss'


class Modal extends Component {

    render() {
        const { headerText, text, onClick, modalActive } = this.props

        return (
            <>
                <div onClick={onClick} className={ modalActive ? "modal-container activ-modal-container" : "modal-container"}>
                    <div className="modal-wrapper" onClick={e=>e.stopPropagation()}>
                        <header className="header-modal">
                            <h3  className="modal-title">{headerText}</h3>
                            <button onClick={onClick} className="btn-close btn-close-white" aria-label="Close">x</button>
                        </header>

                        <div className="modal-body">
                            <div className="modal-text">{text}</div>

                            <div className="button-container">
                                <button  className="button-ok button-modal" onClick={onClick}>Ok</button>
                                <button className="button-cancel button-modal" onClick={onClick}>Cancel</button> 
                            </div>
                        </div>

                    </div>
                </div>
            </>
        )
    }
}



export default Modal
