import axios from 'axios';

const getProductList = () => axios({
    method: "get",
    url: "http://localhost:3000/products.json"
})

export default getProductList


