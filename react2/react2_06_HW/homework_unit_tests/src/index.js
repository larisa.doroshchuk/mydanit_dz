import React from 'react';
import './index.css';
import App from './App';
import reportWebVitals from "./reportWebVitals";
import { createRoot } from 'react-dom/client';
import { Provider } from "react-redux";
import store from './store/store';
import ChangeTopicProvider from './context/ChangeTopicProvider';


const container = document.getElementById('root');
const root = createRoot(container);

root.render(
  <React.StrictMode>
    <Provider store={store}>
      <ChangeTopicProvider>
        <App />
      </ChangeTopicProvider>
    </Provider>
  </React.StrictMode>
);

