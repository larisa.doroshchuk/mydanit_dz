import Product from '../product/Product';
import ProductForTable from '../product_for_table/ProductForTable';
import ModalOk from '../modal_ok/ModalOk';
import './List.css';
import PropTypes from 'prop-types'
import React from 'react';
import { useSelector } from 'react-redux';
//+
import { useChange, TOPIC_CARD, TOPIC_TABLE } from '../../context/ChangeTopicProvider'


const List = (props) => {
    const { handleClick, handleClickFavorite, handleClickClose, handleClickOk, handleClickProduct } = props;
    let productList = useSelector((state) => state.productList);

    const isChange = useChange();

    return (

        <>
            <div className="home-list_container">
                <div className="home-list_wrapper-heder">
                    <div className="home-list_-help"></div>
                    <h3 className="home-list_-title"> Product list</h3>
                    <div className="home-list_button">
                        <button onClick={() => isChange.change(TOPIC_CARD)} className="change-topic" >Cards</button>
                        <button onClick={() => isChange.change(TOPIC_TABLE)} className="change-topic change-topic-table" >Table</button>
                    </div>
                </div>

                {(isChange.topic == TOPIC_CARD) &&
                    <section className="wrapper-item">

                        {productList.map((item) => (

                            <React.Fragment key={item.article}>

                                <div className="home-list_item-card">
                                    <Product
                                        name={item.name}
                                        price={item.price}
                                        color={item.color}
                                        article={item.article}
                                        url={item.url}
                                        handleClick={handleClick}
                                        handleClickClose={handleClickClose}
                                        handleClickOk={handleClickOk}
                                        handleClickFavorite={handleClickFavorite}
                                        handleClickProduct={handleClickProduct}

                                    />
                                </div>
                            </React.Fragment>

                        ))}

                    </section>}

                {(isChange.topic == TOPIC_TABLE) &&
                    <section className="wrapper-item-table">

                        {productList.map((item) => (

                            <React.Fragment key={item.article}>

                                <div className="home-list_item-table">
                                    <ProductForTable
                                        name={item.name}
                                        price={item.price}
                                        color={item.color}
                                        article={item.article}
                                        url={item.url}
                                        handleClick={handleClick}
                                        handleClickClose={handleClickClose}
                                        handleClickOk={handleClickOk}
                                        handleClickFavorite={handleClickFavorite}
                                        handleClickProduct={handleClickProduct}

                                    />
                                </div>
                            </React.Fragment>

                        ))}

                    </section>}


                <ModalOk
                    onClick={handleClickClose}
                />

            </div>

        </>
    )



}


List.propTypes = {
    productList: PropTypes.array,
    handleClick: PropTypes.func,
    handleClickClose: PropTypes.func,
    handleClickOk: PropTypes.func,
    handleClickFavorite: PropTypes.func,
    handleClickProduct: PropTypes.func,
}

export default List
