import React, { } from 'react'
import Modal from '../modal/Modal'
import './Basket.css';
import PropTypes from 'prop-types'
import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom';


const Basket = (props) => {
    const { handleClickModal, handleClickClose, handleClickOk } = props;

    const products = useSelector((state) => state.productList);
    const cartList = useSelector((state) => state.cartList);
    const carts = [];
    let total = 0;

    products.forEach(product => {

        let productQuantity = cartList.filter(elem => product.article === elem.id);

        if (productQuantity.length > 0) {
            product.quantity = productQuantity[0].quantity;
            total += product.quantity * product.price;
            carts.push(product);
        }
    });



    return (
        <>
            <div className="basket-container">
                <h3 className="basket-title"> My Basket</h3>


                {carts.map((item) => (

                    <React.Fragment key={item.article}>

                        <div className="basket-item">
                            <div className="basket-container-item">
                                <p className="basket-id">ID-{item.article}</p>
                                <div className="foto-basket">
                                    <img className="img-basket" src={item.url} alt={item.name} />
                                </div>
                                <div className="basket-info-product">
                                    <h4 className="basket-title-item">{item.name}</h4>
                                    <p className="basket-color">Color: {item.color}</p>
                                </div>
                                <p className="basket-price">{item.price}$ </p>
                                <p className="basket-price">-{item.quantity}-</p>
                                <p className="basket-price">  {item.price * item.quantity}$</p>

                                <button className="basket-close" data-article={item.article} data-name={item.name} onClick={handleClickModal}>X

                                </button>

                            </div>
                        </div>
                    </React.Fragment>

                ))}

                <Modal
                    onClick={handleClickClose}
                    handleClickOk={handleClickOk}
                    handleClickModal={handleClickModal}
                />



                <div className="basket-total-container">
                    <div className="basket-container-item">
                        <div className="basket-estimated-total">Estimated Total - $ {total}</div>
                    </div>

                    <div className="basket_checkout" data-comp="ActionButtons ">
                        <Link to="/basketForm" className="header-home-link">
                            <button data-at="basket_checkout_btn" type="button" className="basket_checkout_btn" data-comp="StyledComponent BaseComponent ">Items for payment</button>
                        </Link>
                    </div>

                </div>

            </div>
        </>
    )
}

export default Basket

Basket.propTypes = {
    cartList: PropTypes.array,
    products: PropTypes.array,
    handleClickModal: PropTypes.func,
    modalActive: PropTypes.any,
    handleClickClose: PropTypes.func,
    handleClickOk: PropTypes.func,

}
