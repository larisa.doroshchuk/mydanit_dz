import { render, screen } from '@testing-library/react'
import Modal from './Modal'
import { Provider } from 'react-redux'
import { legacy_createStore as createStore } from 'redux'
import reducer from '../../store/modalActive/reducer'
import userEvent from '@testing-library/user-event'

describe('<Modal /> component', () => {
    it('should render Modal', () => {
        const store = createStore(reducer, {
            modalActive: true,
            modalContent: {
                name: 'TectName',
                article: 23
            }
        })

        render(
            <Provider store={store}>
                <Modal />
            </Provider>
        )
        screen.getByText(/Are you sure you want to remove this item from your cart/)
    })


    it('should pass onClick if button is clicked', () => {
        const store = createStore(reducer, {
            modalActive: true,
            modalContent: {
                name: 'TectName',
                article: 23
            }
        })

        const handleClick = jest.fn()
        const handleClickOk = jest.fn()

        render(
            <Provider store={store}>
                <Modal handleClickOk={handleClickOk} onClick={handleClick}></Modal>
            </Provider>

        )

        userEvent.click(screen.getAllByRole('button')[0])
        userEvent.click(screen.getAllByRole('button')[1])
        expect(handleClick).toHaveBeenCalled()
        expect(handleClickOk).toHaveBeenCalled()

    })
})
