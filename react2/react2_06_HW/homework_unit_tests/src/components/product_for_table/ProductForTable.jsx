import React, { } from 'react'
import Button from '../button/Button'
import PropTypes from 'prop-types'
import './ProductForTable.css'
import { useSelector } from 'react-redux';


const ProductForTable = (props) => {
    const { name, price, color, article, url, handleClickFavorite, handleClickProduct } = props;
    const favoritesList = useSelector((state) => state.favoritesList);


    return (
        <>
            <section className="product-table_container">
                <div className="product-table_favorites">
                    <p className="product-table_id">ID-{article}</p>
                    <button className="product-table_select-favorites" data-id={article} onClick={handleClickFavorite}>
                        <span id={"favorite-" + article} data-id={article} className={favoritesList.filter(item => +item === +article).length > 0 ? "favorites-icon favorites-icon88" : "favorites-icon favorites-icon89"}></span>
                    </button>
                </div>
                <div className="product-table_foto">
                    <img className="product-table_img" src={url} alt={name} />
                </div>
                <h3 className="product-table_title">{name}</h3>
                <p className="product-table_color">Color: {color}</p>
                <p className="product-table_price">${price}</p>
                <div className="product-table_button">
                    <Button
                        onClick={handleClickProduct}
                        article={article}
                    />
                </div>



            </section>
        </>
    )

}


ProductForTable.propTypes = {
    name: PropTypes.string,
    price: PropTypes.number,
    color: PropTypes.string,
    article: PropTypes.number,
    url: PropTypes.string,
    handleClickFavorite: PropTypes.func,
    favoritesList: PropTypes.any,
    handleClickProduct: PropTypes.func,

}

export default ProductForTable
