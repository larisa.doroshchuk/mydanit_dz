import Product from '../product/Product';
import ProductForTable from '../product_for_table/ProductForTable';
import ModalOk from '../modal_ok/ModalOk';
import PropTypes from 'prop-types'
import './FavoritesList.css';
import React from 'react';
import { useSelector } from 'react-redux';
//+
import { useChange, TOPIC_CARD, TOPIC_TABLE } from '../../context/ChangeTopicProvider'

const FavoritesList = (props) => {
    const { handleClick, handleClickClose, handleClickOk, handleClickFavorite, handleClickModal, handleClickProduct } = props;
    const productList = useSelector((state) => state.productList);
    const favoritesList = useSelector((state) => state.favoritesList);
    let favorites = productList.filter(element => favoritesList.includes(element.article));

    const isChange = useChange();


    return (

        <div className="favoritlist-container">
            <div className="favoritlist-wrapper-heder">
                <div className="favoritlist-help"></div>
                <h3 className="favoritlist-title"> My favorites list</h3>
                <div className="favoritlist-help home-list_button">
                    <button onClick={() => isChange.change(TOPIC_CARD)} className="change-topic" >Cards</button>
                    <button onClick={() => isChange.change(TOPIC_TABLE)} className="change-topic" >Table</button>
                </div>
            </div>


            {(isChange.topic == TOPIC_CARD) &&
                <div className="wrapper-item">

                    {favorites.map((item) => (

                        <React.Fragment key={item.article}>
                            <div className="favoritlist-item-card">
                                <Product
                                    name={item.name}
                                    price={item.price}
                                    color={item.color}
                                    article={item.article}
                                    url={item.url}
                                    handleClick={handleClick}
                                    handleClickClose={handleClickClose}
                                    handleClickOk={handleClickOk}
                                    handleClickFavorite={handleClickFavorite}
                                    handleClickModal={handleClickModal}
                                    handleClickProduct={handleClickProduct}

                                />
                            </div>
                        </React.Fragment>

                    ))}

                </div>}


            {(isChange.topic == TOPIC_TABLE) &&
                <div className="wrapper-item-table">

                    {favorites.map((item) => (

                        <React.Fragment key={item.article}>
                            <div className="favoritlist-item-table">
                                <ProductForTable
                                    name={item.name}
                                    price={item.price}
                                    color={item.color}
                                    article={item.article}
                                    url={item.url}
                                    handleClick={handleClick}
                                    handleClickClose={handleClickClose}
                                    handleClickOk={handleClickOk}
                                    handleClickFavorite={handleClickFavorite}
                                    handleClickModal={handleClickModal}
                                    handleClickProduct={handleClickProduct}

                                />
                            </div>
                        </React.Fragment>

                    ))}

                </div>}

            <ModalOk
                onClick={handleClickClose}
            />

        </div>

    )
}

export default FavoritesList

FavoritesList.propTypes = {
    productList: PropTypes.array,
    favoritesList: PropTypes.array,
    handleClick: PropTypes.func,
    handleClickClose: PropTypes.func,
    handleClickOk: PropTypes.func,
    handleClickFavorite: PropTypes.func,
    handleClickModal: PropTypes.func,
    handleClickProduct: PropTypes.func,
}
