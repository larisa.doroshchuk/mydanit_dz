import { render, screen } from '@testing-library/react'
import ModalOk from './ModalOk'
import { Provider } from 'react-redux'
import { legacy_createStore as createStore } from 'redux'
import reducer from '../../store/modalActive/reducer'
import userEvent from '@testing-library/user-event'

describe('<ModalOk /> component', () => {
    it('should render ModalOk', () => {
        const store = createStore(reducer, {
            modalActive: true
        })

        render(
            <Provider store={store}>
                <ModalOk />
            </Provider>
        )
        screen.getByText(/Your item has been successfully added to the cart/)
    })

    it('should pass onClick if button is clicked', () => {
        const store = createStore(reducer, {
            modalActive: true
        })

        const handleClick = jest.fn()

        render(
            <Provider store={store}>
                <ModalOk onClick={handleClick}>OK</ModalOk>
            </Provider>

        )

        userEvent.click(screen.getByRole('button'))
        expect(handleClick).toHaveBeenCalled()

    })
})
