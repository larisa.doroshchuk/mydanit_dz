import React, { } from 'react'
import List from '../list/List'

const Home = (props) => {

  const { handleClick, handleClickClose, handleClickOk, handleClickFavorite, handleClickProduct } = props;


  return (
    <>
      <List
        handleClick={handleClick}
        handleClickClose={handleClickClose}
        handleClickOk={handleClickOk}
        handleClickFavorite={handleClickFavorite}
        handleClickProduct={handleClickProduct}

      />
    </>
  )


}

export default Home


