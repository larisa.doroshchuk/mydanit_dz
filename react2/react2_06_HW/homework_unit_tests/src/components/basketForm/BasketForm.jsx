import React from 'react';
import { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { deleteCartList } from '../../store/cartList/actions';
import { Formik, Field, Form, ErrorMessage } from 'formik';
import { object, string, number } from 'yup';
import './BasketForm.css';
import { Link } from 'react-router-dom';
import NumberFormat from 'react-number-format';

const CustomErrorMessage = ({ name }) => (
    <ErrorMessage name={name}>
        {message => (
            <div className='error-'>
                <i className='error-message'>{message}</i>
            </div>
        )}
    </ErrorMessage>
)

const loginSchema = object({
    username: string().required('Name is required'),
    userfullname: string().required('Fullname is required'),
    userage: number().required('Age is required').min(2, 'Age is too short'),
    useraddress: string().required('Address is required').min(7, 'Phone is too short'),
    userphone: number().required('Phone is required')
})





const BasketForm = (props) => {

    const [formErrors, setFormErrors] = useState([])
    const dispatch = useDispatch();
    const cartList = useSelector((state) => state.cartList);
    const productList = useSelector((state) => state.productList);

    const handleSubmit = (values, actions) => {
        actions.setSubmitting(true)
        setFormErrors([])
        console.log("User information: username", values.username, ", userfullname ", values.userfullname, ", userage ", values.userage, ", useraddress ", values.useraddress, ", userphone ", values.userphone);

        console.log('productList: ');
        productList.forEach(product => {

            let productQuantity = cartList.filter(elem => product.article === elem.id);

            if (productQuantity.length > 0) {
                console.log(
                    'product: ' + product.name + ' quantity:' + productQuantity[0].quantity
                );
            }
        });
        dispatch(deleteCartList(cartList));
        values.username = "";
        values.userfullname = "";
        values.userage = "";
        values.useraddress = "";
        values.userphone = "";
    }


    return (
        <Formik
            initialValues={{
                username: '',
                userfullname: '',
                userage: '',
                useraddress: '',
                userphone: ''
            }}
            onSubmit={handleSubmit}
            validationSchema={loginSchema}
        >
            {propsFormik => {
                return (
                    <Form className="login-form login-form-container">
                        <div className="formik-wrapper">
                            <div className="formik-blok">
                                <label className='label' htmlFor="username">Name</label>
                                <Field id="username" name="username" />

                                <CustomErrorMessage name="username" />
                            </div>
                            <div className="formik-blok">
                                <label className='label' htmlFor="userfullname">Fullname</label>
                                <Field id="userfullname" name="userfullname" />

                                <CustomErrorMessage name="userfullname" />
                            </div>
                            <div className="formik-blok">
                                <label className='label' htmlFor="userage">Age</label>
                                <Field id="userage" name="userage" />

                                <CustomErrorMessage name="userage" />
                            </div>
                            <div className="formik-blok">
                                <label className='label' htmlFor="useraddress">Address</label>
                                <Field id="useraddress" name="useraddress" />

                                <CustomErrorMessage name="useraddress" />
                            </div>
                            <div className="formik-blok">
                                <label className='label' htmlFor="userphone">Phone</label>
                                <NumberFormat id="userphone" name="userphone" format="+1 (###) ###-####" mask="_"
                                    value={propsFormik.values.userphone}
                                    onValueChange={(values) => {
                                        const { value } = values;
                                        if (value != propsFormik.values.userphone) {
                                            propsFormik.setFieldValue('userphone', value);
                                        }
                                    }}

                                />

                                <CustomErrorMessage name="userphone" />
                            </div>

                            {formErrors.length > 0 &&
                                <ul>
                                    {formErrors.map(error => (
                                        <li key={error} className='error'>{error}</li>
                                    ))}
                                </ul>
                            }
                            <div className="formik-blok-button">
                                <button disabled={propsFormik.isSubmitting} className='submit-btn formik-btn' type='submit'>Checkout</button>
                                <Link to="/" className="header-home-link formik-btn home-link-formik">Home</Link>
                            </div>
                        </div>
                    </Form>
                )
            }}
        </Formik>
    )

}

export default BasketForm
