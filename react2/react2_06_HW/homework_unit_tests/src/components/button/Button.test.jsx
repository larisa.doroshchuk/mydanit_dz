import { render } from '@testing-library/react'
import Button from './Button'

describe('<Button /> component', () => {
    it('should render', () => {
        const { container } = render(<Button className="button-someone" />)
        // console.log(container.innerHTML);

        expect(container.firstChild).toMatchSnapshot()
    })
})
