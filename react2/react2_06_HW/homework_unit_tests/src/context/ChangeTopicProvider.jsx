import React, { useContext, useState } from 'react';

export const TOPIC_CARD = 'card';
export const TOPIC_TABLE = 'table';

const ChangeTopicContext = React.createContext();

export const ChangeTopicProvider = ({ children, ...props }) => {
    const [topic, setTopic] = useState(TOPIC_CARD);

    const change = name => {
        setTopic(name);
    }

    return (

        <ChangeTopicContext.Provider value={{
            topic: topic,
            change: change
        }}
            {...props}
        >
            {children}
        </ChangeTopicContext.Provider>


    )
}

export default ChangeTopicProvider;

export const useChange = () => useContext(ChangeTopicContext);
