import reducer from './reducer';

describe('favorites reducer', () => {
    const state = [
        { "id": 1, "quantity": 2 },
        { "id": 2, "quantity": 1 }
    ];

    it('should return default value', () => {
        expect(reducer(undefined, {})).toEqual([])
    })

    describe('for INIT_CARTLIST', () => {
        it('should init cart', () => {
            expect(reducer(state, {
                type: 'INIT_CARTLIST', payload: {
                    cart: state
                }
            })).toEqual(state)
        })
    })

    describe('for ADD_CARTLIST action', () => {
        it('should add cart', () => {
            expect(reducer(state, {
                type: 'ADD_CARTLIST',
                payload: { cart: 3 }
            })).toEqual([
                { "id": 1, "quantity": 2 },
                { "id": 2, "quantity": 1 },
                { "id": 3, "quantity": 1 }
            ])
        })

        it('should increase the number', () => {
            expect(reducer(state, {
                type: 'ADD_CARTLIST', payload: {
                    cart: 2
                }
            }
            )).toEqual([
                { "id": 1, "quantity": 2 },
                { "id": 2, "quantity": 2 }
            ])
        })

        describe('for REMOVE_CARTLIST', () => {
            it('should remove cart', () => {
                expect(reducer(state, {
                    type: 'REMOVE_CARTLIST', payload: { id: 2 }
                })).toEqual([
                    { "id": 1, "quantity": 2 }
                ])
            })
        })

    })

})