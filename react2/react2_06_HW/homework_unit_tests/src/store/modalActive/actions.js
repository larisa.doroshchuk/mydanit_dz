export const switchModal = enabler => {
    return {
        type: 'SWITCH_MODAL',
        payload: { enabler }
    }
}

