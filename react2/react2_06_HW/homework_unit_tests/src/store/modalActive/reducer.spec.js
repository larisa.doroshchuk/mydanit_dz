import reducer from './reducer';

describe('for SWITCH_MODAL reducer', () => {
    const state = [false];

    it('should toggle state modal', () => {
        expect(reducer(state, {
            type: 'SWITCH_MODAL',
            payload: { enabler: true }
        })).toEqual(true)
    })
})