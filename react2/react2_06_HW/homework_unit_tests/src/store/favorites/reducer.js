const reducer = (state = [], action) => {
    switch (action.type) {
        case 'ADD_FAVORITE': {
            const isExist = state.some((favorit) => { return favorit == action.payload.favorit })
            if (isExist || !action.payload.favorit) return state;
            return [...state, ...action.payload.favorit];
        }
        case 'REMOVE_FAVORITE': {
            return state.filter((elem) => elem != action.payload.id);
        }
        case 'INIT_FAVORITE': {
            return !action.payload.favorit ? [] : [...action.payload.favorit];
        }
        default: {
            return state
        }
    }
}

export default reducer

