export const addFavorite = favorit => {
    return {
        type: 'ADD_FAVORITE',
        payload: { favorit }
    }
}

export const removeFavorite = id => {
    return {
        type: 'REMOVE_FAVORITE',
        payload: { id }
    }
}

export const initFavorite = favorit => {
    return {
        type: 'INIT_FAVORITE',
        payload: { favorit }
    }
}
