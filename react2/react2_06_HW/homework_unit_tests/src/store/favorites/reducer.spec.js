import reducer from './reducer';

describe('favorites reducer', () => {
    const state = [1, 2];

    it('should return default value', () => {
        expect(reducer(undefined, {})).toEqual([])
    })

    describe('for INIT_FAVORITE', () => {
        it('should init favorite', () => {
            expect(reducer(state, {
                type: 'INIT_FAVORITE', payload: {
                    favorit: [3, 4]
                }
            })).toEqual([3, 4])
        })
    })

    describe('for ADD_FAVORITE action', () => {
        it('should add favorite', () => {
            expect(reducer(state, {
                type: 'ADD_FAVORITE',
                payload: {
                    favorit: [3]
                }
            })).toEqual([1, 2, 3])
        })

        it('should not add duplicated favorite', () => {
            expect(reducer(state, {
                type: 'ADD_FAVORITE', payload: {
                    favorit: [2]
                }
            }
            )).toEqual([1, 2])
        })

        describe('for REMOVE_FAVORITE', () => {
            it('should remove favorite', () => {
                expect(reducer(state, {
                    type: 'REMOVE_FAVORITE', payload: { id: 1 }
                })).toEqual([2])
            })
        })

    })

})