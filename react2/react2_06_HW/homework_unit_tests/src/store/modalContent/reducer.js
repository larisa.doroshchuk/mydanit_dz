const reducer = (state = [], action) => {
    switch (action.type) {
        case 'SWITCH_CONTENT_MODAL': {
            return action.payload.content;
        }
        default: {
            return state
        }
    }
}

export default reducer

