import reducer from './reducer';

describe('for SWITCH_CONTENT_MODAL reducer', () => {
    const state = {
        name: '',
        article: 0
    };

    it('should save content for modal', () => {
        expect(reducer(state, {
            type: 'SWITCH_CONTENT_MODAL',
            payload: {
                content: {
                    name: 'loren 3',
                    article: 2
                }
            }
        })).toEqual({
            name: 'loren 3',
            article: 2
        }
        )
    })
})
