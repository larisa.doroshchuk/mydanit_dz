import reducer from './reducer';

describe('for SET_PRODUCT reducer', () => {
    const state = [];

    it('should save content for modal', () => {
        expect(reducer(state, {
            type: 'SET_PRODUCT',
            payload: {
                product: [

                    {
                        "name": "MERIT. Shade Slick Tinted Lip Oil",
                        "price": 24,
                        "url": "https://www.sephora.com/productimages/sku/s2489599-main-zoom.jpg?imwidth=1224",
                        "article": 16,
                        "color": "Taupe - neutral brown"
                    },

                    {
                        "name": "Fenty Beauty by Rihanna. Gloss Bomb Universal Lip Luminizer",
                        "price": 20,
                        "url": "https://www.sephora.com/productimages/sku/s1925965-main-zoom.jpg?imwidth=1224",
                        "article": 1,
                        "color": "Fenty Glow - shimmering rose nude"
                    }
                ]
            }
        })).toEqual([

            {
                "name": "MERIT. Shade Slick Tinted Lip Oil",
                "price": 24,
                "url": "https://www.sephora.com/productimages/sku/s2489599-main-zoom.jpg?imwidth=1224",
                "article": 16,
                "color": "Taupe - neutral brown"
            },

            {
                "name": "Fenty Beauty by Rihanna. Gloss Bomb Universal Lip Luminizer",
                "price": 20,
                "url": "https://www.sephora.com/productimages/sku/s1925965-main-zoom.jpg?imwidth=1224",
                "article": 1,
                "color": "Fenty Glow - shimmering rose nude"
            }
        ]
        )
    })

})
