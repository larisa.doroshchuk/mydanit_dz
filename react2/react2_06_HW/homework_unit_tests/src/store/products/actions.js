import getProductList from '../../api/products';

export const setProduct = product => {
    return {
        type: 'SET_PRODUCT',
        payload: { product }
    }
}

export const fetchProducts = () => {
    return async (dispatch, getState) => {
        const { data } = await getProductList();
        dispatch(setProduct(data));
    }
}
