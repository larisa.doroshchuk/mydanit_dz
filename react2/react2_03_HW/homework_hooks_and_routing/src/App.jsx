import React, { useEffect, useState } from 'react'
import Header from './components/header/Header';
import Home from './components/home/Home';
import Basket from './components/basket/Basket';
import FavoritesList from './components/favoritesList/FavoritesList';
import './App.css';
import { BrowserRouter as Router, Routes, Route } from "react-router-dom"


const App = () => {

  const [modalActive, setModalActive] = useState(false);
  const [productList, setProductList] = useState([]);
  const [cartList, setCartList] = useState([]);
  const [favoritesList, setFavoritesList] = useState([]);
  const [modalContent, setmodalContent] = useState({
    name: '',
    article: 0
  });

  const handleClickProduct = (event) => {
    let cartListNew = [...cartList, +(event.target.id)];
    setCartList(cartListNew);
    setModalActive(true);
    localStorage.setItem('cartList', JSON.stringify(cartListNew));
    console.log(cartListNew);
  };

  const handleClickModal = (event) => {
    setModalActive(true);
    setmodalContent({ name: event.target.dataset.name, article: +(event.target.dataset.article) })
  };

  const handleClickClose = (event) => {
    setModalActive(false);
  };

  const deletFromCart = (event) => {
    let cartListNew = cartList.filter(item => {
      return item !== +event.target.dataset.article;
    })
    console.log(cartListNew);
    setCartList(cartListNew)
    localStorage.setItem('cartList', JSON.stringify(cartListNew));
    return cartListNew;
  };

  const handleClickOk = (event) => {
    let cartListNew = deletFromCart(event);
    setModalActive(false);
    setCartList(cartListNew);
    localStorage.setItem('cartList', JSON.stringify(cartList));
  };

  const handleClickFavorite = (event) => {
    let favoritesListNew = favoritesList;
    let filteredFavorites = favoritesListNew.filter(item => item !== +(event.target.dataset.id));
    favoritesListNew = filteredFavorites.length === favoritesList.length && event.target.dataset.id !== null ? [...favoritesList, +(event.target.dataset.id)] : [...filteredFavorites];
    setFavoritesList(favoritesListNew);
    localStorage.setItem('favoritesList', JSON.stringify(favoritesListNew));
  };

  useEffect(() => {
    const getProductList = () => {
      return fetch('/products.json')
        .then((response) => response.json())
        .then((productList) => {
          setProductList(productList);
          setCartList(localStorage.getItem('cartList') != null ? JSON.parse(localStorage.getItem('cartList')) : []);
          setFavoritesList(localStorage.getItem('favoritesList') != null ? JSON.parse(localStorage.getItem('favoritesList')) : []);
        })
    };
    getProductList();

  }, []);

  return (
    <>
      <Router>
        <Header />
        <Routes>
          <Route exact path="/" element={<Home
            products={productList}
            cartList={cartList}
            favoritesList={favoritesList}
            handleClickProduct={handleClickProduct}
            modalActive={modalActive}
            handleClickClose={handleClickClose}
            handleClickOk={handleClickOk}
            handleClickFavorite={handleClickFavorite}

          />} />
          <Route exact path="/basket" element={<Basket
            products={productList}
            cartList={cartList}
            favoritesList={favoritesList}
            modalActive={modalActive}
            handleClickClose={handleClickClose}
            handleClickOk={handleClickOk}
            handleClickFavorite={handleClickFavorite}
            deletFromCart={deletFromCart}
            handleClickModal={handleClickModal}
            modalName={modalContent.name}
            modalArticle={modalContent.article}
          />} />
          <Route exact path="/favoritesList" element={<FavoritesList
            products={productList}
            cartList={cartList}
            favoritesList={favoritesList}
            modalActive={modalActive}
            handleClickClose={handleClickClose}
            handleClickOk={handleClickOk}
            handleClickFavorite={handleClickFavorite}
            handleClickProduct={handleClickProduct}
            deletFromCart={deletFromCart}
            handleClickModal={handleClickModal}
          />} />
        </Routes>
      </Router>

    </>
  )

}

export default App

