import React, { } from 'react'
import Button from '../button/Button'
import PropTypes from 'prop-types'
import './Product.css'


const Product = (props) => {

    const { name, price, color, article, url, handleClickProduct, handleClickFavorite, favoritesList } = props;

    return (
        <>
            <section className="product-container">
                <div className="product-favorites">
                    <p className="product-id">ID - {article}</p>
                    <button className="select-favorites" data-id={article} onClick={handleClickFavorite}>
                        <span id={"favorite-" + article} data-id={article} className={favoritesList.filter(item => item == article).length > 0 ? "favorites-icon favorites-icon88" : "favorites-icon favorites-icon89"}></span>
                    </button>
                </div>
                <div className="foto-product">
                    <img className="img-product" src={url} alt={name} />
                </div>
                <h3 className="product-title">{name}</h3>
                <p className="product-color">Color: {color}</p>
                <p className="product-price">$ {price}</p>
                <div className="product-button">
                    <Button
                        backgroundColor="grey"
                        text="Add to cart"
                        onClick={handleClickProduct}
                        article={article}
                    />
                </div>



            </section>
        </>
    )

}


Product.propTypes = {
    name: PropTypes.string,
    price: PropTypes.number,
    color: PropTypes.string,
    article: PropTypes.number,
    url: PropTypes.string,
    handleClickFavorite: PropTypes.func,
    favoritesList: PropTypes.any,
    handleClickProduct: PropTypes.func,

}

export default Product
