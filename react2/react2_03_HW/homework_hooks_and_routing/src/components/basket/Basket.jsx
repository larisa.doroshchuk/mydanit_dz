import React, { } from 'react'
import Modal from '../modal/Modal'
import './Basket.css';
import PropTypes from 'prop-types'

const Basket = (props) => {
    const { cartList, products, handleClickModal, modalActive, handleClickClose, handleClickOk, modalName, modalArticle } = props;

    let carts = products.filter(element => {
        return cartList.includes(element.article);
    })

    let cartsCount = arr => arr.reduce((sum, { price }) => sum + price, 0)
    const totalCartsCount = cartsCount(carts)



    return (
        <>
            <div className="basket-container">
                <h3 className="basket-title"> My Basket</h3>

                {carts.map((item) => (

                    <React.Fragment key={item.article}>

                        <div className="basket-item">
                            <div className="basket-container-item">
                                <p className="basket-id">ID - {item.article}</p>
                                <div className="foto-basket">
                                    <img className="img-basket" src={item.url} alt={item.name} />
                                </div>
                                <div className="basket-info-product">
                                    <h4 className="basket-title-item">{item.name}</h4>
                                    <p className="basket-color">Color: {item.color}</p>
                                </div>
                                <p className="basket-price">$ {item.price}</p>

                                <button className="basket-close" data-article={item.article} data-name={item.name} onClick={handleClickModal}>X

                                </button>

                            </div>
                        </div>
                    </React.Fragment>

                ))}

                <Modal
                    article={modalArticle}
                    modalActive={modalActive}
                    headerText="Are you sure you want to remove this item from your cart?"
                    text={modalName}
                    onClick={handleClickClose}
                    handleClickOk={handleClickOk}
                    handleClickModal={handleClickModal}
                />



                <div className="basket-total-container">
                    <div className="basket-container-item">
                        <div className="basket-estimated-total">Estimated Total - $ {totalCartsCount}</div>
                    </div>

                    <div className="basket_checkout" data-comp="ActionButtons ">
                        <button data-at="basket_checkout_btn" type="button" className="basket_checkout_btn" data-comp="StyledComponent BaseComponent ">Items for payment</button>

                    </div>

                </div>

            </div>
        </>
    )
}

export default Basket

Basket.propTypes = {
    cartList: PropTypes.array,
    products: PropTypes.array,
    handleClickModal: PropTypes.func,
    modalActive: PropTypes.any,
    handleClickClose: PropTypes.func,
    handleClickOk: PropTypes.func,

}
