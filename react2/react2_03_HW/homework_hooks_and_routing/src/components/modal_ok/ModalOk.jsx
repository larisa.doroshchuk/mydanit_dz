import React, { } from 'react'
import './ModalOk.css'
import PropTypes from 'prop-types'


const ModalOk = (props) => {

    const { onClick, modalActive } = props;

    return (
        <>
            <div onClick={onClick} className={modalActive ? "modal-ok-container activ-modal-ok-container" : "modal-ok-container"}>
                <div className="modal-wrapper" onClick={e => e.stopPropagation()}>
                    <header className="header-modal">
                        <h3 className="modal-title">Your item has been successfully added to the cart!</h3>
                    </header>

                    <div className="modal-body">
                        <div className="button-container">
                            <button className="button-cancel button-modal" onClick={onClick}>OK</button>
                        </div>
                    </div>

                </div>
            </div>
        </>
    )

}


ModalOk.propTypes = {
    onClick: PropTypes.func,
    modalActive: PropTypes.any,
}


export default ModalOk

