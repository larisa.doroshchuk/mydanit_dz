import React, { } from 'react'
import Product from '../product/Product';
import ModalOk from '../modal_ok/ModalOk';
import PropTypes from 'prop-types'
import './FavoritesList.css';


const FavoritesList = (props) => {
    const { products: productList, favoritesList, handleClick, modalActive, handleClickClose, handleClickOk, handleClickFavorite, handleClickModal, deletFromCart, handleClickProduct } = props;
    console.log(productList);
    let favorites = productList.filter(element => favoritesList.includes(element.article));
    console.log(favorites);

    return (

        <div className="favoritlist-container">
            <h3 className="favoritlist-title"> My favorites list</h3>

            {favorites.map((item) => (

                <React.Fragment key={item.article}>
                    <div className="favoritlist-item">
                        <Product
                            name={item.name}
                            price={item.price}
                            color={item.color}
                            article={item.article}
                            url={item.url}
                            handleClick={handleClick}
                            modalActive={modalActive}
                            handleClickClose={handleClickClose}
                            handleClickOk={handleClickOk}
                            handleClickFavorite={handleClickFavorite}
                            favoritesList={favoritesList}
                            deletFromCart={deletFromCart}
                            handleClickModal={handleClickModal}
                            handleClickProduct={handleClickProduct}

                        />
                    </div>
                </React.Fragment>

            ))}

            <ModalOk
                modalActive={modalActive}
                onClick={handleClickClose}
            />

        </div>

    )
}

export default FavoritesList

FavoritesList.propTypes = {
    productList: PropTypes.array,
    favoritesList: PropTypes.any,
    handleClick: PropTypes.func,
    modalActive: PropTypes.any,
    handleClickClose: PropTypes.func,
    handleClickOk: PropTypes.func,
    handleClickFavorite: PropTypes.func,
    handleClickModal: PropTypes.func,
    deletFromCart: PropTypes.func,
    handleClickProduct: PropTypes.func,
}
