import React, { } from 'react'
import './Modal.css'
import PropTypes from 'prop-types'


const Modal = (props) => {

    const { headerText, text, onClick, modalActive, article, handleClickOk } = props;
    return (
        <>
            <div onClick={onClick} className={modalActive ? "modal-container activ-modal-container" : "modal-container"}>
                <div className="modal-wrapper" onClick={e => e.stopPropagation()}>
                    <header className="header-modal">
                        <h3 className="modal-title">{headerText}</h3>
                        <button onClick={onClick} className="btn-close btn-close-white btn-visually" aria-label="Close" >x</button>
                    </header>

                    <div className="modal-body">
                        <div className="modal-text">{text}</div>

                        <div className="button-container">
                            <button id={article} className="button-ok button-modal" onClick={handleClickOk} data-article={article}>Ok</button>
                            <button className="button-cancel button-modal" onClick={onClick}>Cancel</button>
                        </div>
                    </div>

                </div>
            </div>
        </>
    )

}


Modal.propTypes = {
    headerText: PropTypes.string,
    text: PropTypes.string,
    onClick: PropTypes.func,
    modalActive: PropTypes.any,
    article: PropTypes.number,
    handleClickOk: PropTypes.func,
}


export default Modal

