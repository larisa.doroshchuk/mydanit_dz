import React, { useEffect } from 'react'
import Header from './components/header/Header';
import Home from './components/home/Home';
import Basket from './components/basket/Basket';
import FavoritesList from './components/favoritesList/FavoritesList';
import './App.css';
import { BrowserRouter as Router, Routes, Route } from "react-router-dom"
import { useDispatch, useSelector } from 'react-redux';
import { addFavorite, removeFavorite, initFavorite } from './store/favorites/actions';
import { addCart, removeCart, initCartList } from './store/cartList/actions';
import { switchModal } from './store/modalActive/actions';
import { switchContentModal } from './store/modalContent/actions';
import { fetchProducts } from './store/products/actions';
import BasketForm from './components/basketForm/BasketForm';
//+


const App = () => {

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchProducts());
    dispatch(initCartList(localStorage.getItem('cartList') != null && JSON.parse(localStorage.getItem('cartList'))));
    dispatch(initFavorite(localStorage.getItem('favoritesList') != null && JSON.parse(localStorage.getItem('favoritesList'))));
  }, []);

  let favoritesList = useSelector((state) => state.favoritesList);


  const handleClickProduct = (event) => {
    dispatch(addCart(+(event.target.id)));
    dispatch(switchModal(true));
  };

  const handleClickModal = (event) => {
    dispatch(switchModal(true));
    dispatch(switchContentModal({ name: event.target.dataset.name, article: +(event.target.dataset.article) }));
  };

  const handleClickClose = (event) => {
    dispatch(switchModal(false));
  };

  const handleClickOk = (event) => {
    dispatch(removeCart(+event.target.dataset.article));
    dispatch(switchModal(false));
  };


  const handleClickFavorite = (event) => {
    let favoritesListNew = favoritesList;
    let filteredFavorites = favoritesListNew.filter(item => item !== +(event.target.dataset.id));
    favoritesListNew = filteredFavorites.length === favoritesList.length && event.target.dataset.id !== null ? [...favoritesList, +(event.target.dataset.id)] : [...filteredFavorites];
    if (filteredFavorites.length === favoritesList.length && event.target.dataset.id !== null) {
      dispatch(addFavorite([+event.target.dataset.id]));
    } else {
      dispatch(removeFavorite(+event.target.dataset.id));
    }
    localStorage.setItem('favoritesList', JSON.stringify(favoritesListNew));
  };


  return (
    <>
      <Router>
        <Header />
        <Routes>
          <Route exact path="/" element={<Home
            handleClickProduct={handleClickProduct}
            handleClickClose={handleClickClose}
            handleClickOk={handleClickOk}
            handleClickFavorite={handleClickFavorite}

          />} />
          <Route exact path="/basket" element={<Basket
            handleClickClose={handleClickClose}
            handleClickOk={handleClickOk}
            handleClickModal={handleClickModal}
          />} />
          <Route exact path="/basketForm" element={<BasketForm
          // handleClickClose={handleClickClose}
          // handleClickOk={handleClickOk}
          // handleClickModal={handleClickModal}
          />} />
          <Route exact path="/favoritesList" element={<FavoritesList
            handleClickClose={handleClickClose}
            handleClickOk={handleClickOk}
            handleClickFavorite={handleClickFavorite}
            handleClickProduct={handleClickProduct}
            handleClickModal={handleClickModal}
          />} />
        </Routes>
      </Router>

    </>
  )

}

export default App

