import Product from '../product/Product';
import ModalOk from '../modal_ok/ModalOk';
import PropTypes from 'prop-types'
import './FavoritesList.css';
import React from 'react';
import { useSelector } from 'react-redux';
//+

const FavoritesList = (props) => {
    const { handleClick, handleClickClose, handleClickOk, handleClickFavorite, handleClickModal, handleClickProduct } = props;
    const productList = useSelector((state) => state.productList);
    const favoritesList = useSelector((state) => state.favoritesList);
    let favorites = productList.filter(element => favoritesList.includes(element.article));


    return (

        <div className="favoritlist-container">
            <h3 className="favoritlist-title"> My favorites list</h3>

            {favorites.map((item) => (

                <React.Fragment key={item.article}>
                    <div className="favoritlist-item">
                        <Product
                            name={item.name}
                            price={item.price}
                            color={item.color}
                            article={item.article}
                            url={item.url}
                            handleClick={handleClick}
                            handleClickClose={handleClickClose}
                            handleClickOk={handleClickOk}
                            handleClickFavorite={handleClickFavorite}
                            handleClickModal={handleClickModal}
                            handleClickProduct={handleClickProduct}

                        />
                    </div>
                </React.Fragment>

            ))}

            <ModalOk
                onClick={handleClickClose}
            />

        </div>

    )
}

export default FavoritesList

FavoritesList.propTypes = {
    productList: PropTypes.array,
    favoritesList: PropTypes.array,
    handleClick: PropTypes.func,
    handleClickClose: PropTypes.func,
    handleClickOk: PropTypes.func,
    handleClickFavorite: PropTypes.func,
    handleClickModal: PropTypes.func,
    handleClickProduct: PropTypes.func,
}
