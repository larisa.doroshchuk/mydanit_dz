export const addCart = cart => {
    return {
        type: 'ADD_CARTLIST',
        payload: { cart }
    }
}

export const removeCart = id => {
    return {
        type: 'REMOVE_CARTLIST',
        payload: { id }
    }
}

export const initCartList = cart => {
    return {
        type: 'INIT_CARTLIST',
        payload: { cart }
    }
}

export const deleteCartList = cartList => {
    return {
        type: 'DELETE_CARTLIST',
        payload: { cartList }
    }
}
