const reducer = (state = [], action) => {
    switch (action.type) {
        case 'SET_PRODUCT': {
            return action.payload.product;
        }
        default: {
            return state
        }
    }
}

export default reducer
