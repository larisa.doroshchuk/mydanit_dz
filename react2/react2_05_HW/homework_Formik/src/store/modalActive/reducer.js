const reducer = (state = [], action) => {
    switch (action.type) {
        case 'SWITCH_MODAL': {
            return action.payload.enabler;
        }
        default: {
            return state
        }
    }
}

export default reducer

