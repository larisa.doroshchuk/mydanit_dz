// ## Задание
// Получить список фильмов серии `Звездные войны`, и вывести на экран список персонажей для каждого из них.

// #### Технические требования:
// - Отправить AJAX запрос по адресу `https://ajax.test-danit.com/api/swapi/films` и получить список всех фильмов серии `Звездные войны`
// - Для каждого фильма получить с сервера список персонажей, которые были показаны в данном фильме. Список персонажей можно получить из свойства `characters`.
// - Как только с сервера будет получена информация о фильмах, сразу же вывести список всех фильмов на экран. Необходимо указать номер эпизода, название фильма, а также короткое содержание (поля `episode_id`, `title` и `opening_crawl`).
// - Как только с сервера будет получена информация о персонажах какого-либо фильма, вывести эту информацию на экран под названием фильма.

// #### Необязательное задание продвинутой сложности
//  - Пока загружаются персонажи фильма, прокручивать под именем фильма анимацию загрузки. Анимацию можно использовать любую. Желательно найти вариант на чистом CSS без использования JavaScript.

// ## Примечание
// Задание должно быть выполнено на "чистом" Javascript без использования библиотек типа jQuery или React.

// #### Литература:
// - [Использование Fetch на MDN](https://developer.mozilla.org/ru/docs/Web/API/Fetch_API/Using_Fetch)
// - [Fetch](https://learn.javascript.ru/fetch)
// - [CSS анимация](https://html5book.ru/css3-animation/)
// - [События DOM](https://learn.javascript.ru/introduction-browser-events)

////////////////////////////////////////

const root = document.getElementById('root');

const requestmovieList = fetch('https://ajax.test-danit.com/api/swapi/films');

const listEl = document.createElement('div');
root.append(listEl);

requestmovieList
    .then(response => {
        return response.json();
    })
    .then(requestmovieList => {
        //console.log(requestmovieList);
        const list = requestmovieList.map(({ name: title, episodeId: episode_id, openingCrawl: opening_crawl, characters }) => {
            // console.log(characters);
            const div = document.createElement('div');
            const h2 = document.createElement('h2');
            const h4 = document.createElement('h4');
            const p = document.createElement('p');
            let ol = document.createElement('ol');
            ShowListCharacters(characters, ol);

            h2.innerText = "Movie title - " + title;

            h4.innerText = "Episode - " + episode_id;
            p.innerText = "Short description :  " + opening_crawl;
            div.append(h2, ol, h4, p);
            return div;
        })
        listEl.append(...list);
    })


function ShowListCharacters(arr, ol) {


    arr.forEach(element => {

        let character = fetch(element);

        character
            .then(response => {
                return response.json();
            })
            .then(character => {
                let li = document.createElement('li');
                li.innerText = character.name;
                ol.append(li);
            })

    });
}

