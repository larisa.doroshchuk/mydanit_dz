// ## Задание
// Написать программу "Я тебя по айпи вычислю"

// #### Технические требования:
// - Создать простую HTML страницу с кнопкой `Вычислить по IP`.
// - По нажатию на кнопку - отправить AJAX запрос по адресу `https://api.ipify.org/?format=json`, получить оттуда IP адрес клиента.
// - Узнав IP адрес, отправить запрос на сервис `https://ip-api.com/` и получить информацию о физическом адресе.
// - Под кнопкой вывести на страницу информацию, полученную из последнего запроса - континент, страна, регион, город, район города.
// - Все запросы на сервер необходимо выполнить с помощью async await.

// ## Примечание
// Задание должно быть выполнено на "чистом" Javascript без использования библиотек типа jQuery или React.

// #### Литература:
// - [Async/await](https://learn.javascript.ru/async-await)
// - [async function](https://developer.mozilla.org/ru/docs/Web/JavaScript/Reference/Statements/async_function)
// - [Документация сервиса ip-api.com](http://ip-api.com/docs/api:json)

//////////////////////////////////////////////////////////////////////////////

const root = document.getElementById('root');
const btn = document.createElement('button');
btn.className = 'btn';
btn.textContent = 'Вычислить по IP';
root.append(btn);


btn.addEventListener('click', () => calculateByIP());

async function calculateByIP() {
    const requestIpUsers = await fetch('https://api.ipify.org/?format=json');
    getDataByIP(await requestIpUsers.json());
}

async function getDataByIP(requestIpUsers) {
    const requestUserAddressFetch = await fetch(`http://ip-api.com/json/${requestIpUsers.ip}?fields=message,continent,country,regionName,city,district`);
    const requestUserAddress = await requestUserAddressFetch.json();
    const userAddress = document.createElement('span');
    userAddress.className = 'user-address';
    userAddress.textContent = `Kонтинент : ${requestUserAddress.continent},   Cтрана : ${requestUserAddress.country},   Pегион : ${requestUserAddress.regionName},   Город : ${requestUserAddress.city},   Район города : ${requestUserAddress.district}`;
    root.append(userAddress);
}
