const root = document.getElementById('root');

class UsersCard {

    constructor(title, content, name, email, idPost) {
        this.title = title;
        this.content = content;
        this.name = name;
        this.email = email;
        this.idPost = idPost;
    };
    createCard() {
        const div = document.createElement('div');
        div.className = 'user-card';

        const spanName = document.createElement('span');
        spanName.className = 'user-name';
        spanName.textContent = this.name;

        const spanEmail = document.createElement('span');
        spanEmail.className = 'user-email';
        spanEmail.textContent = this.email;

        const icon = document.createElement('i');
        icon.classList.add('bi', 'bi-trash-fill', 'user-close');
        icon.id = `icon-${this.idPost}`;
        icon.addEventListener("click", () => {
            fetch(`https://ajax.test-danit.com/api/json/posts/${this.idPost}`, { method: 'DELETE' })
                .then(response => {
                    return response.status;
                })
                .then(deletedUser => {
                    if (deletedUser !== 404) {
                        div.remove();
                    }
                })
        })

        const title = document.createElement('h1');
        title.className = 'title-card';
        title.textContent = this.title;

        const content = document.createElement('p');
        content.className = 'content-card';
        content.textContent = this.content;


        div.append(spanName, spanEmail, icon, title, content);
        return div;
    }
}

const requestUsersList = fetch('https://ajax.test-danit.com/api/json/users');

requestUsersList
    .then(response => {
        return response.json();
    })
    .then(requestUsersList => {
        let usersList = [];
        requestUsersList.forEach(user => {
            usersList[user.id] = user;
        })
        console.log(usersList);
        getPostList(usersList);
    });

function getPostList(allUsersList) {
    const requestPostsList = fetch('https://ajax.test-danit.com/api/json/posts');

    requestPostsList
        .then(response => {
            return response.json();
        })
        .then(requestPostsList => {
            requestPostsList.forEach(post => {
                const userCartPost = new UsersCard(post.title, post.body, allUsersList[post.userId].name, allUsersList[post.userId].email, post.id);
                root.append(userCartPost.createCard());
            })
        });
}
