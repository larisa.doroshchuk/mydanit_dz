// ## Задание

// - Выведите этот массив на экран в виде списка (тег ul - список должен быть сгенерирован с помощью Javascript).
// - На странице должен находиться `div` с `id="root"`, куда и нужно будет положить этот список (похожая задача была дана в модуле basic). 
// - Перед выводом обьекта на странице, нужно проверить его на корректность (в объекте должны содержаться все три свойства - author, name, price). Если какого-то из этих свойств нету, в консоли должна высветиться ошибка с указанием - какого свойства нету в обьекте. 
// - Те элементы массива, которые являются некорректными по условиям предыдущего пункта, не должны появиться на странице.

// #### Примечание
// Задание должно быть выполнено на "чистом" Javascript без использования библиотек типа jQuery или React.

// #### Литература:
// - [Перехват ошибок, "try..catch"](https://learn.javascript.ru/exception)
// - [try...catch на MDN](https://developer.mozilla.org/uk/docs/Web/JavaScript/Reference/Statements/try...catch)

"use strict";

const books = [
    {
        author: "Скотт Бэккер",
        name: "Тьма, что приходит прежде",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Воин-пророк",
    },
    {
        name: "Тысячекратная мысль",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Нечестивый Консульт",
        price: 70
    },
    {
        author: "Дарья Донцова",
        name: "Детектив на диете",
        price: 40
    },
    {
        author: "Дарья Донцова",
        name: "Дед Снегур и Морозочка",
    }
];

const wrapper = document.getElementById("root");

function showList(arr, wrapper) {

    const ul = document.createElement(`ul`);
    wrapper.append(ul);

    arr.forEach(function (obj) {
        try {
            if (!("author" in obj)) {
                throw "Свойства author нет в объекте:" + JSON.stringify(obj);
            }

            if (!("name" in obj)) {
                throw "Свойства name нет в объекте:" + JSON.stringify(obj);
            }

            if (!("price" in obj)) {
                throw "Свойства price нет в объекте:" + JSON.stringify(obj);
            }

            const li = document.createElement(`li`);
            li.innerHTML = `${obj.author}, ${obj.name}, ${obj.price} `;
            ul.append(li);

        } catch (error) {
            console.error(error);
        }
    })

};

showList(books, wrapper);

