// ## Задание
// 1. Реализовать класс Employee, в котором будут следующие свойства - name (имя), age (возраст), salary (зарплата). Сделайте так, чтобы эти свойства заполнялись при создании объекта. 
// 2. Создайте геттеры и сеттеры для этих свойств.
// 3. Создайте класс Programmer, который будет наследоваться от класса Employee, и у которого будет свойство lang (список языков).
// 4. Для класса Programmer перезапишите геттер для свойства salary. Пусть он возвращает свойство salary, умноженное на 3.
// 5. Создайте несколько экземпляров обьекта Programmer, выведите их в консоль.

// ## Примечание
// Задание должно быть выполнено на "чистом" Javascript без использования библиотек типа jQuery или React.

// #### Литература:
// - [Классы на MDN](https://developer.mozilla.org/ru/docs/Web/JavaScript/Reference/Classes)
// - [Классы в ECMAScript 6](https://frontender.info/es6-classes-final/)

class Employee {
    constructor(name, age, salary) {
        this.name = name;
        this.age = age;
        this.salary = salary;
    }

    setName(name) {
        this.name = name;
    }

    getName() {
        return this.name;
    }

    setAge(age) {
        this.age = age;
    }

    getAge() {
        return this.age;
    }

    setSalary(salary) {
        this.salary = salary;
    }

    getSalary() {
        return this.salary;
    }
}

class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this.lang = lang;
    }

    getSalary() {
        return this.salary * 3;
    }
}

const worker = new Employee("Lara", 20, 800);

const devSteve = new Programmer("Steve", 30, 30000, ["js", "C++"]);
const devJenna = new Programmer("Jenna", 25, 5000, ["Js", "PHP"]);

[worker, devSteve, devJenna].forEach(e => console.log(e, e.getSalary()));
