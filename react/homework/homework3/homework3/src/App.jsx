import React, { useEffect, useState } from 'react'
import Header from './components/header/Header';
import Home from './components/home/Home';
import Basket from './components/basket/Basket';
import FavoritesList from './components/favoritesList/FavoritesList';
import './App.css';
import { BrowserRouter as Router, Routes, Route } from "react-router-dom"


const App = () => {

  const [modalActive, setModalActive] = useState(false);
  const [productList, setProductList] = useState([]);
  const [chartList, setChartList] = useState({});
  const [favoritesList, setFavoritesList] = useState({});


  const handleClickProduct = (event) => {
    let chartListNew = chartList;
    chartListNew[event.target.id] = getNumberProduct(event.target.id);
    setModalActive(false);
    setChartList(chartListNew);
    setChartList((chartList) => ({ ...chartList, chartListNew }));
    localStorage.setItem('chartList', JSON.stringify(chartList));
  };

  const handleClickModal = (event) => {
    setModalActive(event.target.dataset.article);
  };



  const handleClickClose = (event) => {
    setModalActive(false);

  };

  const deletFromChart = (event) => {
    delete chartList[event.target.dataset.article]
    localStorage.setItem('chartList', JSON.stringify(chartList));
    return chartList;
  }

  const handleClickOk = (event) => {

    let chartListNew = deletFromChart(event);
    setModalActive(false);
    setChartList(chartListNew);
    localStorage.setItem('chartList', JSON.stringify(chartList));
    console.log(chartList); // оставила для наглядности процесса

  };

  const getNumberProduct = (id) => {
    return typeof chartList[id] === "undefined" ? 1 : chartList[id] + 1;
  };

  const handleClickFavorite = (event) => {
    let favoritesListNew = favoritesList;
    favoritesListNew[event.target.id] = changeFavorites(event.target.id);
    setFavoritesList((favoritesList) => ({ ...favoritesList, favoritesListNew }));
    localStorage.setItem('favoritesList', JSON.stringify(favoritesList));
  }

  const changeFavorites = (id) => {
    return typeof favoritesList[id] === "undefined" || favoritesList[id] === false ? true : false;
  };

  useEffect(() => {
    const getProductList = () => {
      return fetch('/products.json')
        .then((response) => response.json())
        .then((productList) => {
          setProductList(productList);
          setChartList(localStorage.getItem('chartList') != null ? JSON.parse(localStorage.getItem('chartList')) : {});
          setFavoritesList(localStorage.getItem('favoritesList') != null ? JSON.parse(localStorage.getItem('favoritesList')) : {});
        })
    };
    getProductList();

  }, []);

  return (
    <>
      <Router>
        <Header />
        <Routes>
          <Route exact path="/" element={<Home
            products={productList}
            chartList={chartList}
            favoritesList={favoritesList}
            handleClickProduct={handleClickProduct}
            modalActive={modalActive}
            handleClickClose={handleClickClose}
            handleClickOk={handleClickOk}
            handleClickFavorite={handleClickFavorite}

          />} />
          <Route exact path="/basket" element={<Basket
            products={productList}
            chartList={chartList}
            favoritesList={favoritesList}
            modalActive={modalActive}
            handleClickClose={handleClickClose}
            handleClickOk={handleClickOk}
            handleClickFavorite={handleClickFavorite}
            deletFromChart={deletFromChart}
            handleClickModal={handleClickModal}
          />} />
          <Route exact path="/favoritesList" element={<FavoritesList
            products={productList}
            chartList={chartList}
            favoritesList={favoritesList}
            modalActive={modalActive}
            handleClickClose={handleClickClose}
            handleClickOk={handleClickOk}
            handleClickFavorite={handleClickFavorite}
            handleClickProduct={handleClickProduct}
            deletFromChart={deletFromChart}
            handleClickModal={handleClickModal}
          />} />
        </Routes>
      </Router>

    </>
  )

}

export default App

