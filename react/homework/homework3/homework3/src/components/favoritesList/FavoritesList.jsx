import React, { } from 'react'
import Product from '../product/Product';
import PropTypes from 'prop-types'
import './FavoritesList.css';


const FavoritesList = (props) => {
    const { products: productList, favoritesList, handleClick, modalActive, handleClickClose, handleClickOk, handleClickFavorite, handleClickModal, deletFromChart, handleClickProduct } = props;

    let favorites = productList.filter(element => {
        return favoritesList["favorite-" + element.article];
    })


    return (

        <div className="favoritlist-container">
            <h3 className="favoritlist-title"> My favorites list</h3>

            {favorites.map((item) => (

                <React.Fragment key={item.article}>
                    <div className="favoritlist-item">
                        <Product
                            name={item.name}
                            price={item.price}
                            color={item.color}
                            article={item.article}
                            url={item.url}
                            handleClick={handleClick}
                            modalActive={modalActive}
                            handleClickClose={handleClickClose}
                            handleClickOk={handleClickOk}
                            handleClickFavorite={handleClickFavorite}
                            favoritesList={favoritesList}
                            deletFromChart={deletFromChart}
                            handleClickModal={handleClickModal}
                            handleClickProduct={handleClickProduct}

                        />
                    </div>
                </React.Fragment>

            ))}

        </div>

    )
}

export default FavoritesList

// products: productList, favoritesList, handleClick, modalActive, handleClickClose, handleClickOk, handleClickFavorite, handleClickModal, deletFromChart, handleClickProduct } = props;

FavoritesList.propTypes = {
    productList: PropTypes.array,
    favoritesList: PropTypes.object,
    handleClick: PropTypes.func,
    modalActive: PropTypes.any,
    handleClickClose: PropTypes.func,
    handleClickOk: PropTypes.func,
    handleClickFavorite: PropTypes.func,
    handleClickModal: PropTypes.func,
    deletFromChart: PropTypes.func,
    handleClickProduct: PropTypes.func,
}
