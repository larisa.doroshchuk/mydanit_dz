import React, { } from 'react'
import List from '../list/List'

const Home = (props) => {

  const { products: productList, favoritesList, handleClick, modalActive, handleClickClose, handleClickOk, handleClickFavorite, chartList, handleClickProduct } = props;


  return (
    <>
      <List
        products={productList}
        chartList={chartList}
        favoritesList={favoritesList}
        handleClick={handleClick}
        modalActive={modalActive}
        handleClickClose={handleClickClose}
        handleClickOk={handleClickOk}
        handleClickFavorite={handleClickFavorite}
        handleClickProduct={handleClickProduct}
      />
    </>
  )


}

export default Home


