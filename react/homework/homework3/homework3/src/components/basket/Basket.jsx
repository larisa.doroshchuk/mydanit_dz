import React, { } from 'react'
import Modal from '../modal/Modal'
import './Basket.css';
import PropTypes from 'prop-types'

const Basket = (props) => {
    const { chartList, products, handleClickModal, modalActive, handleClickClose, handleClickOk } = props;


    let charts = products.filter(element => {
        return chartList[element.article];
    })

    let chartsCount = arr => arr.reduce((sum, { price }) => sum + price, 0)
    const totalChartsCount = chartsCount(charts)



    return (
        <>
            <div className="basket-container">
                <h3 className="basket-title"> My Basket</h3>



                {charts.map((item) => (

                    <React.Fragment key={item.article}>

                        <div className="basket-item">
                            <div className="basket-container-item">
                                <p className="basket-id">ID - {item.article}</p>
                                <div className="foto-basket">
                                    <img className="img-basket" src={item.url} alt={item.name} />
                                </div>
                                <div className="basket-info-product">
                                    <h4 className="basket-title-item">{item.name}</h4>
                                    <p className="basket-color">Color: {item.color}</p>
                                </div>
                                <p className="basket-price">$ {item.price}</p>

                                <button className="basket-close" data-article={item.article} onClick={handleClickModal}>X

                                </button>

                                <Modal
                                    article={item.article}
                                    modalActive={modalActive}
                                    headerText="Are you sure you want to remove this item from your cart?"
                                    text={item.name}
                                    onClick={handleClickClose}
                                    handleClickOk={handleClickOk}
                                    handleClickModal={handleClickModal}
                                />

                            </div>
                        </div>
                    </React.Fragment>

                ))}



                <div className="basket-total-container">
                    <div className="basket-container-item">
                        <div className="basket-estimated-total">Estimated Total - $ {totalChartsCount}</div>
                    </div>

                    <div className="basket_checkout" data-comp="ActionButtons ">
                        <button data-at="basket_checkout_btn" type="button" className="basket_checkout_btn" data-comp="StyledComponent BaseComponent ">Items for payment</button>

                    </div>

                </div>

            </div>
        </>
    )
}

export default Basket



Basket.propTypes = {
    chartList: PropTypes.object,
    products: PropTypes.array,
    handleClickModal: PropTypes.func,
    modalActive: PropTypes.any,
    handleClickClose: PropTypes.func,
    handleClickOk: PropTypes.func,

}
