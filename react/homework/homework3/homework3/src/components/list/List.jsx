import React, { } from 'react'
import Product from '../product/Product';
import './List.css'
import PropTypes from 'prop-types'


const List = (props) => {

    const { products: productList, favoritesList, handleClick, modalActive, handleClickClose, handleClickOk, handleClickFavorite, handleClickProduct } = props;


    return (

        <>
            <section className="list-container">

                {productList.map((item) => (

                    <React.Fragment key={item.article}>
                        <div className="list-item">
                            <Product
                                name={item.name}
                                price={item.price}
                                color={item.color}
                                article={item.article}
                                url={item.url}
                                handleClick={handleClick}
                                modalActive={modalActive}
                                handleClickClose={handleClickClose}
                                handleClickOk={handleClickOk}
                                handleClickFavorite={handleClickFavorite}
                                favoritesList={favoritesList}
                                handleClickProduct={handleClickProduct}

                            />
                        </div>
                    </React.Fragment>

                ))}

            </section>

        </>
    )



}


List.propTypes = {
    productList: PropTypes.array,
    favoritesList: PropTypes.object,
    handleClick: PropTypes.func,
    modalActive: PropTypes.any,
    handleClickClose: PropTypes.func,
    handleClickOk: PropTypes.func,
    handleClickFavorite: PropTypes.func,
    handleClickProduct: PropTypes.func,
}

export default List