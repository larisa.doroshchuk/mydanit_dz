import React, { Component } from 'react';
import Modal from './components/Modal';
import Button from './components/Button';


class App extends Component {

  constructor(props) {
    super(props)
    this.state = {
      modalActive: false,
      modalActiveTwo: false,
    }
  }

  handleClick = (event) => {
    this.setState({ modalActive: true });
  }

  handleClickTwo = (event) => {
    this.setState({ modalActiveTwo: true });
  }

  handleClickClose = (event) => {
    this.setState({ modalActive: false });

  }

  handleClickCloseTwo = (event) => {
    this.setState({ modalActiveTwo: false });

  }

  render() {
    return (
            <>
              <Button
                backgroundColor="red"
                text="Open first modal"
                onClick={this.handleClick}
              />
              <Button
                backgroundColor="blue"
                text="Open second modal"
                onClick={this.handleClickTwo}
                
              />
              <Modal modalActive={this.state.modalActive} headerText="Do you want to delete this file?" text="Once you delete this file, won't be possible to undo this action. Are you sure you want to delete it?" onClick={this.handleClickClose}/>
              <Modal modalActive={this.state.modalActiveTwo} headerText="Do you want to add this file?" text="Is this fight right for you? Confirm that you want to debug this file." onClick={this.handleClickCloseTwo}/>
            </>
     
    )
  }
}

export default App






// import logo from './logo.svg';
// import './App.css';
// import './components/Button'

// function App() {
//   return (
//     <div className="App">
//       <header className="App-header">
//         <img src={logo} className="App-logo" alt="logo" />
//         <p>
//           Edit <code>!!!!!!src/App.js</code> and save to reload.
//         </p>
//         <a
//           className="App-link"
//           href="https://reactjs.org"
//           target="_blank"
//           rel="noopener noreferrer"
//         >
//           Learn React
//         </a>
//       </header>
//     </div>
//   );
// }

// export default App;
