import React, { Component } from 'react'
import './Button.css'

class Button extends Component {
    
    render() {
        const { text, backgroundColor, onClick } = this.props
        const style={backgroundColor: backgroundColor}

        return (
            <>
                <button onClick={onClick} style={style} className="button-someone">{text}</button>
           </>
        )
    }
}


export default Button
