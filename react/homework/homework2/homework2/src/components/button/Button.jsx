import React, { Component } from 'react'
import PropTypes from 'prop-types'
import './Button.css'

class Button extends Component {
    
    render() {
        const { text, backgroundColor, onClick, article } = this.props
        const style={backgroundColor: backgroundColor}

        return (
            <>
                <button id={article} onClick={onClick} style={style} className="button-someone">{text}</button>
           </>
        )
    }
}


Button.propTypes = {
    backgroundColor: PropTypes.string,
    text: PropTypes.string,
    onClick: PropTypes.func,
    article: PropTypes.number
}

Button.defaultProps = {
    text: ""
}

export default Button
