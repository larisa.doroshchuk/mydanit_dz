import React, { Component } from 'react';
import List from './components/list/List';
import {getProductList} from '../src/api/products'


class App extends Component {

  constructor(props) {
    super(props)
    this.state = {
      modalActive: false,
      modalActiveTwo: false,
      productList: [],
      chartList: {},
      favoritesList: {},
    }
  
  }

  handleClick = (event) => {
    this.setState({ modalActive: event.target.id });
  }

  handleClickTwo = (event) => {
    this.setState({ modalActiveTwo: true });
  }

  handleClickClose = (event) => {
    this.setState({ modalActive: false });
  }

  handleClickOk = (event) => {
    this.state.chartList[event.target.id] = this.getNumberProduct(event.target.id)
    this.setState({ modalActive: false, chartList: this.state.chartList});
    localStorage.setItem('chartList',  JSON.stringify(this.state.chartList));
    console.log(this.state.chartList); // оставила для наглядности процесса
  }

  getNumberProduct = (id) => {
    return typeof this.state.chartList[id] === "undefined" ? 1 : this.state.chartList[id] + 1 ;
  }

  handleClickFavorite = (event) => {
    this.state.favoritesList[event.target.id] = this.changeFavorites(event.target.id)
    this.setState({  favoritesList: this.state.favoritesList});
    localStorage.setItem('favoritesList', JSON.stringify(this.state.favoritesList));

  }

  changeFavorites = (id) => {
    return typeof this.state.favoritesList[id] === "undefined" || this.state.favoritesList[id] === false ?  true: false ;
  }


  componentDidMount() {
    getProductList().then((productList) => this.setState({
      productList: productList, 
      chartList: localStorage.getItem( 'chartList') != null ? JSON.parse(localStorage.getItem( 'chartList')) : {}, 
      favoritesList: localStorage.getItem( 'favoritesList') != null ? JSON.parse(localStorage.getItem( 'favoritesList')) : {}
    }));
  }

    

  render() {
    return (
            <>
              <List
                   productList={this.state.productList} 
                   chartList={this.state.chartList} 
                   favoritesList={this.state.favoritesList}
                   handleClick={this.handleClick}
                   modalActive={this.state.modalActive}
                   handleClickClose={this.handleClickClose}
                   handleClickOk={this.handleClickOk}
                   handleClickFavorite={this.handleClickFavorite}
              />

            </>
     
    )
  }
}

export default App

