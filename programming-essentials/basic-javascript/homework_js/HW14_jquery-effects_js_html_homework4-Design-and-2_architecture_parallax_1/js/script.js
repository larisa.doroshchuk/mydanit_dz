"use strict";

// Задание
// Добавить в домашнее задание HTML/CSS №4 (Flex/Grid) различные эффекты с использованием jQuery

// Технические требования:

// Добавить вверху страницы горизонтальное меню со ссылкой на все разделы страницы.
// При клике на ссылку - плавно прокручивать страницу вниз до выбранного места.
// Если прокрутить страницу больше чем на 1 экран вниз, справа внизу должна появляться кнопка "Наверх" 
// с фиксированным позиционариванием. При клике на нее - плавно прокрутить страницу в самый верх.
// Добавить под одной из секций кнопку, которая будет выполнять функцию slideToggle() 
// (прятать и показывать по клику) для данной секции.

/////////////////////////////////////////////////////////////////

// block: display news , hide news
// Добавить под одной из секций кнопку, которая будет выполнять функцию slideToggle() 
// (прятать и показывать по клику) для данной секции.

$(`.button-hot-news`).click(function slideToggle() {

    if ($(`.hot-news-list`).hasClass(`hot-news-list-remove`)) {
        $(`.hot-news-list`).toggleClass(`hot-news-list-remove`);
        $(`.button-hot-news`).html(`hide news`);

    } else {
        $(`.hot-news-list`).toggleClass(`hot-news-list-remove`);
        $(`.button-hot-news`).html(`display news`);
    }
});


// block: 
// Если прокрутить страницу больше чем на 1 экран вниз, справа внизу должна появляться кнопка "Наверх" 
// с фиксированным позиционариванием. При клике на нее - плавно прокрутить страницу в самый верх.

let button = $('#button');

$(window).scroll(function () {
    if ($(window).scrollTop() > window.innerHeight) {
        button.addClass('show');
    } else {
        button.removeClass('show');
    }
});

button.on('click', function (e) {
    e.preventDefault();
    $('html, body').animate({ scrollTop: 0 }, '300');
});


// block: 
// Добавить вверху страницы горизонтальное меню со ссылкой на все разделы страницы.
// При клике на ссылку - плавно прокручивать страницу вниз до выбранного места.


function anchor(active) {
    if (active == true) {
        let hash = window.location.hash;
        if (hash) {
            $("#menu-section a li").removeClass("section-menu-link-activ");
            $("a[href='" + hash + "'] li").addClass("section-menu-link-activ");
        }

    }
    function anchorHref(e) {
        let anchor = $(this);
        let hash = window.location.hash;
        console.log(e.target);
        if (hash) {
            $("#menu-section a li").removeClass("section-menu-link-activ");
            $(e.target).addClass("section-menu-link-activ");
        }
        $('html, body').animate({
            scrollTop: $(anchor.attr("href")).offset().top
        }, 1000);
    }
    $("a[href*='#']").click(anchorHref);
}

anchor(true);





















































// Решение задачи в классе

// Добавить в Html кнопку ScrollToTop.
// По дефолту кнопка не видна.
// Когда пользователь проскроллит больше чем высота окна (window.innerHeight) - показать кнопку.
// Кнопка должна быть фиксированная внизу странички.
// По клику на нее - нужно проскролить в начало документа.
// (https://developer.mozilla.org/ru/docs/Web/API/Window/scrollTo)

//////////////////////////////////

// function scrollToTop(params) {
//     window.scrollTo({
//         top: 0,
//         behavior: "smooth",
//     });
// }

// document.addEventListener("scroll", function () {
//     const windowHeight = window.innerHeight;
//     // Сколько проскроллил
//     const scrolled = window.pageYOffset;

//     if (scrolled > windowHeight) {
//         showScrollToTopBtn();
//     } else {
//         hideScrollToTopBtn();
//     }
// });


///////////////////////////////
// window.addEventListener("scroll", function () {
//   // Высота окна
//   const windowHeight = window.innerHeight;
//   // Сколько проскроллил
//   const scrolled = window.pageYOffset;

//   console.log(windowHeight);
//   console.log(scrolled);
//   if (scrolled > windowHeight) {
//     showScrollToTopBtn();
//   } else {
//     hideScrollToTopBtn();
//   }
// });

//////////////////////////////////

// function showScrollToTopBtn() {
//     document.getElementById("scrollToTop").classList.add("active");
// }

// function hideScrollToTopBtn() {
//     document.getElementById("scrollToTop").classList.remove("active");
// }

// document.getElementById("scrollToTop").addEventListener("click", scrollToTop);
