"use strict";

let span = document.createElement("span");

const btn = document.createElement("button");
btn.innerText = "x";

const correct = document.createElement("p");

const nameInput = document.createElement("span");
nameInput.innerText = "Price:  ";
document.body.append(nameInput);

const input = document.createElement("input");
input.type = "text";
input.id = "inp";
input.value = "";
document.body.append(input);

input.addEventListener("focus", imputBorderGreen);
input.addEventListener("blur", imputBorderGreenPrice);
btn.addEventListener("click", inputDelete);

function imputBorderGreen(event) {
    input.style.border = "3px solid green";
    input.style.color = "black";
    correct.remove();
}


function imputBorderGreenPrice(event) {
    span.innerText = "";
    input.style.border = "";
    correct.innerText = "";
    if (input.value === "" || isNaN(+input.value) || +input.value < 0) {
        correct.innerText = "";
        correct.innerText = "Please enter correct price.";
        document.body.append(correct);
        input.style.border = "3px solid red";

    } else if (typeof +input.value === "number") {
        span.innerText = `Текущая цена:  ${input.value}          `;
        document.body.prepend(span);
        span.append(btn);
        const lineBreak = document.createElement("br");
        span.append(lineBreak);
        input.style.color = "green";
    };
};


btn.addEventListener("click", inputDelete);

function inputDelete(event) {
    input.value = "";
    input.style.backgroundColor = "";
    span.remove();
    btn.remove();
}
