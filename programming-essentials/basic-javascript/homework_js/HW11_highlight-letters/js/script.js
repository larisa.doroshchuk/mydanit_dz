"use strict";


// В файле index.html лежит разметка для кнопок.
// Каждая кнопка содержит в себе название клавиши на клавиатуре
// По нажатию указанных клавиш - та кнопка, на которой написана эта буква, 
// должна окрашиваться в синий цвет.При этом, если какая - то другая буква 
// уже ранее была окрашена в синий цвет - она становится черной.
// Например по нажатию Enter первая кнопка окрашивается в синий цвет.
// Далее, пользователь нажимает S, и кнопка S окрашивается в синий цвет, 
// а кнопка Enter опять становится черной.

////////////////////////////////////////////////////////////////////////

// Решение без Data  атрибутов по кнопкам:

// let btn = document.querySelectorAll(".btn");

// function removeClass(list) {

//     list.forEach(function (element) {
//         element.classList.remove("active");
//     })
// }


// btn.forEach(element => {

//     element.addEventListener("keydown", function (event) {

//         if (event.key == element.innerHTML) {
//             removeClass(btn);
//             element.classList.add("active");
//         };
//     })
// });


///////////////////////////////////////////////////////////////////////

// Решение с Data  атрибутами  на весь экран:
let btn = document.querySelectorAll(".btn");

function removeClass(list) {

    list.forEach(function (element) {
        element.classList.remove("active");
    })
};

window.addEventListener("keyup", function (event) {

    btn.forEach(element => {
        if (event.key === element.dataset.keyname) {
            removeClass(btn);
            element.classList.add("active");
        };
    })
});

/////////////////////////////////////////////////////////////////////