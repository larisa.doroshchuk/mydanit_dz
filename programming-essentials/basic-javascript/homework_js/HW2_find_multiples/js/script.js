"use strict";

let numberOne = prompt("Please enter any integer greater than zero");

while (isNaN(+numberOne) || numberOne === "" || numberOne === null || +numberOne < 5 || +numberOne % 1 !== 0) {
    if (numberOne === null) {
        numberOne = "";
    }
    if (+numberOne < 5) {
        alert("Sorry, no numbers");
    }
    numberOne = prompt("Please enter any integer greater than zero - correct!", numberOne);
}

for (let i = 5; i <= +numberOne; i++) {
    if (i % 5 === 0) {
        console.log(i);
    }
}
