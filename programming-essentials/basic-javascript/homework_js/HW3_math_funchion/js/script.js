"use strict";

function isCorrectNumber(number) {
    return number !== null && !isNaN(+number) && number.trim() !== "";
}

let numberOne = prompt("Please enter any first number");
while (!isCorrectNumber(numberOne)) {
    if (numberOne === null) {
        numberOne = "";
    }
    numberOne = prompt("Please enter the first number correctly!", numberOne);

}

let numberTwo = prompt("Please enter any second number");
while (!isCorrectNumber(numberTwo)) {
    if (numberTwo === null) {
        numberTwo = "";
    }
    numberTwo = prompt("Please enter the second number correctly!", numberTwo);
}

let mathOperation = prompt("Select a math operation: (+) or (-) or (*) or (/).");
while (mathOperation !== "+" && mathOperation !== "-" && mathOperation !== "*" && mathOperation !== "/") {
    mathOperation = prompt("You did not specify the mathematical operation exactly! Select only: (+), (-), (*) or (/).");
}


function calcMathOperation(one, two, mathOp) {

    let result;
    switch (mathOp) {
        case "+":
            result = (+one) + (+two);
            return (`${one} ${mathOp} ${two} =  ${result}`);

        case "-":
            result = one - two;
            return (`${one} ${mathOp} ${two} =  ${result}`);

        case "*":
            result = one * two;
            return (`${one} ${mathOp} ${two} =  ${result}`);

        case "/":
            if ((+two) === 0) {
                return ("You cannot divide by zero!");
            }
            result = one / two;
            return (`${one} ${mathOp} ${two} =  ${result}`);

    }
}

console.log(calcMathOperation(numberOne, numberTwo, mathOperation));






