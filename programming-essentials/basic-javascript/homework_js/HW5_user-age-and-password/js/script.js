"use strict";

function createNewUser() {

    let firstName = prompt("Please enter any first name");
    let lastName = prompt("Please enter any last name");
    let birthday = prompt("Please enter birthday (dd.mm.yyyy)");

    const newUser = {
        _firstName: firstName,
        _lastName: lastName,
        _birthday: birthday.split("."),


        setFirstName(value) {
            this._firstName = value;
        },

        setLastName(value) {
            this._lastName = value;
        },

        getLogin() {
            return `${this._firstName.substr(0, 1)}${this._lastName}`.toLowerCase();
        },

        getAge() {

            let currentDate = new Date();
            let birthdayDate = new Date(this._birthday[2], this._birthday[1] - 1, this._birthday[0]);

            let age;
            if ((currentDate - birthdayDate) >= 0) {
                return Math.floor((currentDate - birthdayDate) / 86400000 / 365);
            };
            if (currentDate < birthdayDate) {
                return Error;
            };
            if ((currentDate - getMilliseconds(birthdayDate)) < 0) {
                return Math.floor(((currentDate - birthdayDate) / 86400000 / 365 * (-1)) + (currentDate / 86400000 / 365));
            };
        },

        getPassword() {
            return (
                `${this._firstName.substr(0, 1)}`.toUpperCase() +
                `${this._lastName}`.toLowerCase() +
                this._birthday[2]
            );

        }

    }

    Object.defineProperty(newUser, '_firstName', {
        writable: false
    })

    Object.defineProperty(newUser, '_lastName', {
        writable: false
    })

    return newUser;
}

const newUser = createNewUser();
console.log(newUser.getLogin());

console.log(` newUser.getAge() - Вам  ${newUser.getAge()}  лет/года`);
console.log(` newUser.getPassword() -   ${newUser.getPassword()}`);
