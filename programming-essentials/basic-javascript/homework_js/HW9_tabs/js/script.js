"use strict";

const tabs = document.getElementById("tabs");
const tabsContent = document.getElementById("tabs-content");
let classActiv = document.getElementsByClassName("active");
let classShow = document.getElementsByClassName("show");


tabs.addEventListener("click", event => {
    if (event.target.tagName === "LI") {
        classShow[0].classList.remove("show");
        classActiv[0].classList.remove("active");
        event.target.classList.add("active");
        tabsContent.children[(getIndex(event.target))].classList.add("show");
    }
});

function getIndex(node) {
    let childs = node.parentNode.children;
    for (let i = 0; i < childs.length; i++) {
        if (node === childs[i]) {
            return i
        }
    }
}