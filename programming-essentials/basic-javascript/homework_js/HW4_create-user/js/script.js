"use strict";

function createNewUser() {

    let firstName = prompt("Please enter any first name");
    let lastName = prompt("Please enter any last name");

    const newUser = {
        _firstName: firstName,
        _lastName: lastName,

        setFirstName(value) {
            this._firstName = value;
        },

        setLastName(value) {
            this._lastName = value;
        },

        getLogin() {
            return `${this._firstName.substr(0, 1)}${this._lastName}`.toLowerCase();
        }

    }

    return newUser;
}

const newUser = createNewUser();
console.log(newUser.getLogin());

