"use strict";

const enter = document.getElementById("enter");
const control = document.getElementById("control");
let btn = document.getElementById("btn");
const text = document.createElement("p");

const iconPassword = document.getElementsByClassName("icon-password");
const faEye = document.getElementsByClassName("fa-eye");
const faEyeSlash = document.getElementsByClassName("fa-eye-slash");


iconPassword[0].addEventListener("click", clickIconPassword);
iconPassword[1].addEventListener("click", clickIconPassword);

function clickIconPassword(event) {

    event.target.classList.toggle("fa-eye");

    if (event.target.classList.toggle("fa-eye-slash")) {
        event.target.parentElement.children[0].type = "text";
    } else {
        event.target.parentElement.children[0].type = "password";
    }
}


btn.addEventListener("click", controlEvent);

function controlEvent(event) {

    text.remove();

    if (enter.value !== control.value || enter.value === "") {

        text.innerText = "Нужно ввести одинаковые значения!";
        text.style.color = "red";
        let labelText = document.getElementById("label-text");
        labelText.append(text);

    } else if (enter.value === control.value) {
        event.preventDefault();
        alert("You are welcome!");
    }

};

