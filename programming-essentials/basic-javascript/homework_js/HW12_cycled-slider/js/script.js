"use strict";

// Задание

// Реализовать программу, показывающую циклично разные картинки.
// Задача должна быть реализована на языке javascript,
//     без использования фреймворков и сторонник библиотек(типа Jquery).


// Технические требования:


// В папке banners лежит HTML код и папка с картинками.
// При запуске программы на экране должна отображаться первая картинка.
//     Через 3 секунды вместо нее должна быть показана вторая картинка.
// Еще через 3 секунды - третья.
// Еще через 3 секунды - четвертая.
// После того, как покажутся все картинки - этот цикл должен начаться заново.
// При запуске программы где - то на экране должна появиться кнопка с надписью Прекратить.
// По нажатию на кнопку Прекратить цикл завершается, на экране остается показанной та картинка,
//     которая была там при нажатии кнопки.
// Рядом с кнопкой Прекратить должна быть кнопка Возобновить показ, при нажатии которой цикл
// продолжается с той картинки, которая в данный момент показана на экране.
// Разметку можно менять, добавлять нужные классы, id, атрибуты, теги.

///////////////////////////////

let imadges = document.querySelectorAll(".image-to-show");
let current = 0;
let imagesStop = false;


function slider() {

    if (document.imagesStop) {
        return true;
    }

    for (let i = 0; i < imadges.length; i++) {
        imadges[i].classList.add("activ-img");
    }
    imadges[current].classList.remove("activ-img");

    if (current + 1 == imadges.length) {
        current = 0;
    } else {
        current++;
    }
}


function startProgramCarousel() {

    setInterval(slider, 3000);

    document.querySelector(".to-terminate").classList.add("activ-btn");
    startPauseCarousel();
    stopPauseCarousel();
}


function startPauseCarousel() {
    document.querySelector(".to-terminate").addEventListener("click", function () {
        document.querySelector(".resume-showing").classList.add("activ-btn");
        document.imagesStop = true;
    })
}

function stopPauseCarousel() {
    document.querySelector(".resume-showing").addEventListener("click", function () {
        document.imagesStop = false;
    })
}

startProgramCarousel()
