"use strict";


// Взять любое готовое домашнее задание по HTML/CSS.
// Добавить на макете кнопку "Сменить тему".
// При нажатии на кнопку - менять цветовую гамму сайта (цвета кнопок, фона и т.д.) на ваше усмотрение. 
// При повтором нажатии - возвращать все как было изначально - как будто для страницы доступны
// две цветовых темы.
// Выбранная тема должна сохраняться и после перезагрузки страницы

let themeButton = document.getElementById("theme-button");
let linkCss = document.getElementById("link-css");
let SiteTheme;


themeButton.addEventListener("click", function () {
    if (localStorage.getItem("SiteTheme") === "orange") {
        linkCss.setAttribute("href", "./css/style_blue-theme.css");
        localStorage.setItem("SiteTheme", "blue");
    } else {
        linkCss.setAttribute("href", "./css/style.css");
        localStorage.setItem("SiteTheme", "orange");
    }
});

function checkingUserSelectedTopic(linkCss) {
    if (localStorage.getItem("SiteTheme") == "orange" || localStorage.getItem("SiteTheme") == "blue") {
        gettingTweetedTopicUser(linkCss);
    } else {
        linkCss.setAttribute("href", "./css/style.css");
        SiteTheme = localStorage.setItem("SiteTheme", "orange");
    }
}


function gettingTweetedTopicUser(linkCss) {
    if (localStorage.getItem("SiteTheme") == "orange") {
        linkCss.setAttribute("href", "./css/style.css");
    } else {
        linkCss.setAttribute("href", "./css/style_blue-theme.css");
        localStorage.setItem("SiteTheme", "blue");
    }
};

checkingUserSelectedTopic(linkCss);
