"use strict";

function showList(arr, parentDOM = document.body) {

    const ol = document.createElement(`ol`);
    const divTimer = document.createElement(`div`);
    ol.innerHTML = listTree(arr);

    let secs = 3;
    const timer = setInterval(tick, 1000);

    function tick() {
        if (secs <= 0) {
            clearInterval(timer);
            ol.hidden = true;
            divTimer.hidden = true;
        };
        divTimer.innerHTML = `Осталось '${secs--}' секунд`;
        parentDOM.append(divTimer);
    }
    parentDOM.append(ol);
};


function listTree(arr, string = '') {
    let newArray = arr.map(function (element) {
        if (!Array.isArray(element)) {
            string += `<li>${element}</li>`;
        } else {
            string += `<li><ol>${listTree(element)}</ol></li>`;
        }
    });

    return string;
}


showList(["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", ["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"], "Lviv"]);