"use strict";

function filterBy(arr, arrType) {
    return arr.filter(element =>
        arrType === 'null' && element !== null ||
        arrType === 'object' && (element === null || typeof element !== arrType) ||
        arrType !== 'object' && arrType !== 'null' && typeof element !== arrType
    );
}

console.log(filterBy(['hello', 'world', 23, '23', { "ss": 34 }, null, null, 111, null], 'number'));
