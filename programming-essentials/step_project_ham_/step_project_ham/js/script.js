"use strict";

///////////////////////////////////////////
//  js blok "our-services" click

let servicesList = document.querySelector(".services-list");
let servicesListLink = document.querySelectorAll(".services-link");
let servicesListItem = document.querySelectorAll(".services-item");
let servicesListPointer = document.querySelectorAll(".services-list div img");
let servicesListDiv = document.querySelectorAll(".services-list div");
let servicesWrapper = document.querySelectorAll(".services-wrapper section");

servicesList.addEventListener("click", function (event) {
    hidingContent(servicesWrapper); // + скрываем контент
    hidingLink(servicesListLink);  //  + удаляем класс клика на ссылке
    hidingItem(servicesListItem);   //  + удаляем класс клика на "li"
    hidingPointer(servicesListPointer);  //  + удаляем "img" стрелку указатель на сервис

    if ((event.target.tagName === "LI")) {
        event.target.classList.add("services-item-click");
        event.target.lastElementChild.classList.add("services-link-click");
        event.target.nextElementSibling.classList.add("service-pointer-click");
        selectingTheRightContent(event.target.innerText); //  + отбор нужного контента по клику  по дата-атрибуту
    };
    if ((event.target.tagName === "A")) {
        event.target.classList.add("services-link-click");
        event.target.parentElement.classList.add("services-item-click");
        event.target.offsetParent.children[1].classList.add("service-pointer-click");
        selectingTheRightContent(event.target.innerText); //  + отбор нужного контента по клику  по дата-атрибуту
    };

})

function hidingContent(list) { //  + скрываем контент
    list.forEach(element => {
        element.classList.remove("services-content-click");
    });
};

//  + удаляем класс клика на ссылке
function hidingLink(servicesListLink) {
    servicesListLink.forEach(element => {
        element.classList.remove("services-link-click");
    });
};


//  + удаляем класс клика на "li"
function hidingItem(list) {
    list.forEach(element => {
        element.classList.remove("services-item-click");
    });
};

function hidingPointer(list) { //  + удаляем "img" стрелку указатель на сервис

    list.forEach(element => {
        element.classList.remove("service-pointer-click");
    });
};


function selectingTheRightContent(servic) { //  + отбор нужного контента по клику, по дата-атрибуту

    servicesWrapper.forEach(element => {
        if (element.dataset.services == servic) {
            element.classList.add("services-content-click");
        }
    });
};


////////////////////////////////////////////////////
//  js blok "our-work" click


let workList = document.querySelector(".work-list");
let workListItem = document.querySelectorAll(".work-list li");
let workListLink = document.querySelectorAll(".work-list li a");

let cardsWork = document.querySelectorAll(".cards-work .card-work");

let ourWorkButtonRemove = document.querySelector(".container-button-load");
let cardWorkClickButton = document.querySelector(".container-button-load");
let containerButtonRemove = document.querySelectorAll(".card-work-click-button");


ourWorkButtonRemove.addEventListener("click", function (event) {
    containerButtonRemove = document.querySelectorAll(".card-work-click-button");
    if (containerButtonRemove.length > 12) {
        for (let i = 0; i < 12; i++) {
            containerButtonRemove[i].classList.remove("card-work-click-button");
        }
    }
    if (containerButtonRemove.length <= 12) {
        for (let i = 0; i < containerButtonRemove.length; i++) {
            containerButtonRemove[i].classList.remove("card-work-click-button");
        }
        cardWorkClickButton.classList.add("container-button-remove");
    }
})


workList.addEventListener("click", function (event) {
    hidingCard(cardsWork);      //  + скрываем карты
    hidingLinkWork(workListLink);  //  + удаляем класс клика на ссылке
    hidingItemWork(workListItem); //  + удаляем класс клика на "li"

    if ((event.target.tagName === "LI")) {
        event.target.classList.add("work-item-click");
        event.target.lastElementChild.classList.add("work-link-click");
        selectionGraphicDesign(event.target.innerText); //  + отбор нужного контента по клику  по дата-атрибуту
        if (event.target.innerText === "All") {
            selectionAll(cardsWork);  //  + отображаем все карты
        };
    };

    if ((event.target.tagName === "A")) {
        event.target.classList.add("work-link-click");
        event.target.parentElement.classList.add("work-item-click");
        selectionGraphicDesign(event.target.innerText); //  + отбор нужного контента по клику  по дата-атрибуту
        if (event.target.innerText === "All") {
            selectionAll(cardsWork);  //  + отображаем все карты
        };
    };

});


function hidingCard(list) { //  + скрываем карты
    list.forEach(element => {
        element.classList.remove("card-work-click");
    });
}

function selectionAll(list) {   //  + отображаем все карты
    list.forEach(element => {
        element.classList.add("card-work-click");
    });
}


function selectionGraphicDesign(servic) { //   отбор нужного контента "Graphic Design" по клику,  по дата-атрибуту

    cardsWork.forEach(element => {
        if (element.dataset.cardwork == servic) {
            element.classList.add("card-work-click");
        }
    });
};


function hidingLinkWork(list) { //  + удаляем класс клика на ссылке

    list.forEach(element => {
        element.classList.remove("work-link-click");
    });
};

function hidingItemWork(list) { //  + удаляем класс клика на "li"

    list.forEach(element => {
        element.classList.remove("work-item-click");
    });
};


////////////////////////////////////////////////////
//  js blok "people-say" carousel click

let arrBlockPeopleSay = [
    {
        "idcard": 0,
        "content": "Augue tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis. Tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis.",
        "specialt": "UX Designer",
        "name": "Jack Aldridge",
        "photoMax": "../step_project_ham/images/people_say/0_img_max_specialist.png",
        "photoIcon": "../step_project_ham/images/people_say/0_specialist.png"
    },
    {
        "idcard": 1,
        "content": " Tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis. Integer dignissim, augue tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis.",
        "specialt": "Frontend developer ",
        "name": "Amelia Brown",
        "photoMax": "../step_project_ham/images/people_say/1_img_max_specialist.png",
        "photoIcon": "../step_project_ham/images/people_say/1_specialist.png"
    },
    {
        "idcard": 2,
        "content": "Integer dignissim, augue tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis. Tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis.",
        "specialt": "Full Stack Javascript Developer",
        "name": "Harry Johnson",
        "photoMax": "../step_project_ham/images/people_say/2_img_max_specialist.png",
        "photoIcon": "../step_project_ham/images/people_say/2_specialist.png"
    },
    {
        "idcard": 3,
        "content": " Morbi pulvinar odio eget aliquam facilisis. Tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis. Integer dignissim, augue tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa. ",
        "specialt": "Head of sales",
        "name": "Lily King",
        "photoMax": "../step_project_ham/images/people_say/3_img_max_specialist.png",
        "photoIcon": "../step_project_ham/images/people_say/3_specialist.png"
    },
];


let containerImgMin = $(".container-img-min");
const imgMaxSpecialist = $(".img-max-specialist");
const contentSpecialist = $(".text");
const specialt = $(".specialt");
const namePeople = $(".name-people");

const clickLeft = $(".click-left");
const clickRight = $(".click-right");

containerImgMin.click(changeTheCardOnClick);

function changeTheCardOnClick(e) {

    $(".container-img-min-click").removeClass("container-img-min-click");
    $(e.target.parentElement).addClass("container-img-min-click");

    let currentID = e.target.parentElement.dataset.idcard;
    changeContentByID(currentID);
}


function changeContentByID(id) {

    imgMaxSpecialist.fadeOut('slow', function () { imgMaxSpecialist.attr('src', arrBlockPeopleSay[id].photoMax); imgMaxSpecialist.fadeIn('slow'); });
    contentSpecialist.fadeOut('slow', function () { contentSpecialist.html(arrBlockPeopleSay[id].content); contentSpecialist.fadeIn('slow'); });
    specialt.fadeOut('slow', function () { specialt.html(arrBlockPeopleSay[id].specialt); specialt.fadeIn('slow'); });
    namePeople.html(arrBlockPeopleSay[id].name);
}


clickLeft.click(switchLeft);

function switchLeft(e) {
    let peopleID = $(".container-img-min-click")[0].dataset.idcard;
    if (peopleID == 0) {
        peopleID = 4;
    }
    peopleID--;
    $(".container-img-min-click").removeClass("container-img-min-click");
    $($(".container-img-min")[peopleID]).addClass("container-img-min-click");

    changeContentByID(peopleID);
}


clickRight.click(switchRight);

function switchRight(e) {

    let peopleID = $(".container-img-min-click")[0].dataset.idcard;
    if (peopleID == 3) {
        peopleID = -1;
    }
    peopleID++;
    $(".container-img-min-click").removeClass("container-img-min-click");
    $($(".container-img-min")[peopleID]).addClass("container-img-min-click");

    changeContentByID(peopleID);
}

