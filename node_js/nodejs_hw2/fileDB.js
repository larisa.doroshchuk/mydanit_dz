const Table = require("./table");
const fs = require('fs');
const path = require('path');

class FileDB {
    tables = {};

    registerSchema(tableName, schema) {
        this.tables[tableName] = new Table(tableName, schema);
    }

    getTable(tableName) {
        return this.tables[tableName];
    }
}

module.exports = FileDB;
