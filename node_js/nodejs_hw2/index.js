"use strict";
const FileDB = require("./fileDB");

const newspostSchema = {
    id: Number,
    title: String,
    text: String,
    createDate: Date,
};

const fileDB = new FileDB();
fileDB.registerSchema('newspost', newspostSchema);
const newspostTable = fileDB.getTable('newspost');

async function posts() {
    const newsposts = await newspostTable.getAll();
    console.log("newsposts", newsposts);

    const newspost = await newspostTable.getById(2);
    console.log("newspost", newspost);

    const data = {
        title: 'У зоопарку Чернігова лисичка народила лисеня',
        text: "В Чернігівському заопарку сталася чудова подія! Лисичка на ім'я Руда народила чудове лисенятко! Тож поспішайте навідатись та подивитись на це миле створіння!"
    }
    const createdpost = await newspostTable.create(data);
    console.log("createdpost", createdpost);


    const updatedNewsposts = await newspostTable.update(2, { title: "Маленька лисичка" });
    console.log("updatedNewsposts", updatedNewsposts);

    const deletedId = await newspostTable.delete(3);
    console.log("deletedId", deletedId);

}

posts();
