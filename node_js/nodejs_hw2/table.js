const path = require('path');
const fs = require('fs');

class Table {
    constructor(tableName, schema) {
        this.tableName = tableName;
        this.schema = schema;
        this.filePath = path.join(__dirname, `${tableName}.json`);
        this.updatingData = false;
    }

    async save(data) {
        await fs.promises.writeFile(this.filePath, JSON.stringify(data));
        return data;
    }

    async load() {
        try {
            const data = await fs.promises.readFile(this.filePath, 'utf8');
            return JSON.parse(data);
        } catch (err) {
            return null;
        }
    }

    validateData(data) {
        if (!this.schema) {
            throw new Error(`Схема для таблиці "${this.tableName}" не зареєстрована`);
        }
        for (const field in this.schema) {
            const fieldType = this.schema[field];
            const fieldValue = data[field];
            if (fieldType.name === Number && fieldValue !== 'number') {
                throw new Error(`Fiield "${field}" should be Number`);
            }
            if (fieldType.name === String && fieldValue !== 'string') {
                throw new Error(`Fiield "${field}" should be String`);
            }
            if (fieldType.name === Date && Object.prototype.toString.call(fieldValue) !== '[object Date]') {
                throw new Error(`Fiield "${field}" should be "Date"`);
            }
        }
        return true;
    }

    async getBiggestId(posts) {
        let biggestId = 0;
        for (let index = 0; index < posts.length; index++) {
            if (posts[index]["id"] > biggestId) {
                biggestId = posts[index]["id"];
            }
        }
        return biggestId;
    }

    async create(data) {
        let posts = await this.load() || [];
        let post = {
            id: await this.getBiggestId(posts) + 1,
            title: data['title'],
            text: data['text'],
            createDate: new Date()
        }
        this.validateData(post);
        posts.push(post);
        await this.save(posts);
        return post;
    }

    async getAll() {
        const records = await this.load() || {};
        return records || [];
    }

    async delete(id) {
        let records = await this.load() || [];
        const index = records.findIndex(record => record.id === id);
        if (index !== -1) {
            let deletedId = records[index]["id"];
            records.splice(index, 1);
            await this.save(records);
            return deletedId;
        }
        // throw new Error(`Wrong id: "${id}"`);
        return `Wrong id: "${id}"`;
    }

    async getById(id) {
        const records = await this.load() || [];
        const index = records.findIndex(record => record.id === id);
        if (index !== -1) {
            return records[index];
        }
        return {};
    }

    async update(id, data) {
        let posts = await this.load() || [];
        let updatedId = -1;
        for (let index = 0; index < posts.length; index++) {
            if (posts[index]["id"] === id) {
                if (typeof data["text"] !== "undefined") {
                    posts[index]["text"] = data["text"];
                }
                if (typeof data["title"] !== "undefined") {
                    posts[index]["title"] = data["title"];
                }
                this.validateData(posts[index]);
                updatedId = index;
                break;
            }
        }
        if (updatedId === -1) {
            throw new Error(`Wrong id: "${id}"`);

        }
        await this.save(posts);
        console.log(updatedId);
        return posts[updatedId];
    }
}
module.exports = Table;
