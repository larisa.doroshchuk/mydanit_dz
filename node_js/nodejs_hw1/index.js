const http = require('http');
const url = require('url');

let requestCount = 0;

const server = http.createServer((req, res) => {

    const parsedUrl = url.parse(req.url, true);

    if (parsedUrl.pathname === '/') {
        requestCount++;
        res.setHeader('Content-Type', 'application/json');
        res.end(JSON.stringify({
            message: 'Request handled successfully',
            requestCount: requestCount
        }));
    } else {
        res.statusCode = 404;
        res.end('Not Found');
    }
});

const port = process.argv.find(arg => arg.startsWith('--port=')) || '--port=3000';
const portNumber = parseInt(port.split('=')[1]);
server.listen(portNumber, () => {
    console.log(`Server is running on http://localhost:${portNumber}`);
});


