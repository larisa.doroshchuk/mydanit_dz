const coin = document.getElementById("coin1");
const audio = new Audio("./audio/a2d4fd123353974.mp3");

document.body.onkeyup = function (e) {
    if (e.keyCode == 32) {
        coin.classList.add("coin-show");
        audio.play();
        setTimeout(removeShow, 15000);
    }
}

function removeShow() {
    coin.classList.remove("coin-show");
    randomCoin();
}


function rotateCoin() {
    console.log(coin.src);
    coin.src = coin.src.includes("./images/reverse.png") ? "./images/obverse.png" : "./images/reverse.png";
}

function randomCoin() {
    let random = parseInt(Math.round(Math.random() * 1));
    image = ["url('./images/obverse.png')", "url(./images/reverse.png)"];
    coin.style.backgroundImage = image[random];
    audio.play();
}

